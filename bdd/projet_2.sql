-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 26 Avril 2016 à 23:16
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projet_2`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id_article` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_scenario` int(11) unsigned DEFAULT NULL,
  `id_piece` int(11) unsigned DEFAULT NULL,
  `code_article` varchar(20) DEFAULT NULL,
  `MAJ_article` date DEFAULT NULL,
  `CUPI_article` varchar(3) DEFAULT NULL,
  `surface` float unsigned DEFAULT NULL,
  `poste` varchar(15) DEFAULT NULL,
  `type_materiau` varchar(25) DEFAULT NULL,
  `libelle` text,
  `fabricant` varchar(50) DEFAULT NULL,
  `unite` varchar(5) DEFAULT NULL,
  `prix_unitaire` float unsigned DEFAULT NULL,
  `duree_de_vie` float unsigned DEFAULT NULL,
  `taux_entretien` float unsigned DEFAULT NULL,
  `taux_remplacement` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id_article`),
  KEY `fk_article_scenario` (`id_scenario`),
  KEY `fk_article_piece` (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`id_article`, `id_scenario`, `id_piece`, `code_article`, `MAJ_article`, `CUPI_article`, `surface`, `poste`, `type_materiau`, `libelle`, `fabricant`, `unite`, `prix_unitaire`, `duree_de_vie`, `taux_entretien`, `taux_remplacement`) VALUES
(1, 1, 1, 'PAR-CHC-001', '2016-04-21', '', 10, 'sol', 'parquet', 'Parquet chêne contrecollé 2 plis épaisseur 9,5 mm', '', 'm2', 62.2, 20, 0.008, 1),
(2, 2, 1, 'SSO-PVC-001', '2016-04-21', '', 10, 'sol', 'pvc', 'Lés PVC sur mousse, acoustique 19 dB', 'Tarkett Bâtiment', 'm2', 32, 20, 0.03, 1),
(3, 3, 2, 'CAR-GCM-003', '2016-04-21', '', 30, 'sol', 'carrelage', 'Carrelage grès cérame mat aspect grainé de 30x30cm', '', 'm2', 38, 40, 0.003, 1),
(4, 4, 2, 'PAR-CHC-002', '2016-04-21', '', 30, 'sol', 'parquet', 'Parquet chêne contrecollé, classe 31, ép. 15 mm', '', 'm2', 54.4, 20, 0, 1),
(5, 5, 3, 'EIT-EPM-002', '2016-04-21', '', 130, 'veture', 'enduit', 'Enduit de parement minéral CR finition rustique écrasé', 'Parexlanko', 'm2', 36.9, 50, 0.015, 0.4),
(6, 6, 3, 'EIT-EPM-002', '2016-04-21', '', 65, 'veture', 'enduit', 'Enduit de parement minéral CR finition rustique écrasé', 'Parexlanko', 'm2', 36.9, 50, 0.015, 0.4),
(7, 6, 3, 'RFA-BRI-004', '2016-04-21', '', 65, 'veture', 'brique', 'Revêtement mural extérieur briques pleines 22x10,5x5', '', 'm2', 117, 100, 0.02, 1),
(8, 7, 3, 'RFA-BRI-004', '2016-04-21', '', 130, 'veture', 'brique', 'Revêtement mural extérieur briques pleines 22x10,5x5', '', 'm2', 117, 100, 0.02, 1);

-- --------------------------------------------------------

--
-- Structure de la table `informations`
--

CREATE TABLE IF NOT EXISTS `informations` (
  `id_information` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `taux_inflation` float DEFAULT NULL,
  `duree_exploitation` float unsigned DEFAULT NULL,
  `cout_ext_fixe` double DEFAULT NULL,
  `cout_ext_annuel` double DEFAULT NULL,
  `comment` text,
  `cout_externalite` double unsigned DEFAULT NULL,
  `cout_externalite_courant` double unsigned DEFAULT NULL,
  PRIMARY KEY (`id_information`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `informations`
--

INSERT INTO `informations` (`id_information`, `taux_inflation`, `duree_exploitation`, `cout_ext_fixe`, `cout_ext_annuel`, `comment`, `cout_externalite`, `cout_externalite_courant`) VALUES
(1, 2, 50, 0, 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pieces`
--

CREATE TABLE IF NOT EXISTS `pieces` (
  `id_piece` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom_piece` varchar(50) NOT NULL,
  `surface` float NOT NULL,
  `comment_piece` text,
  PRIMARY KEY (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pieces`
--

INSERT INTO `pieces` (`id_piece`, `nom_piece`, `surface`, `comment_piece`) VALUES
(1, 'Chambre 1', 10, 'Pièce au sud'),
(2, 'Salon', 30, 'Pièce de vie'),
(3, 'Vêture', 130, 'Vêture du bâtiment');

-- --------------------------------------------------------

--
-- Structure de la table `resultats`
--

CREATE TABLE IF NOT EXISTS `resultats` (
  `id_resultat` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_scenario` int(11) unsigned DEFAULT NULL,
  `id_piece` int(11) unsigned DEFAULT NULL,
  `cout_construction` double unsigned DEFAULT NULL,
  `cout_remplacement` double unsigned DEFAULT NULL,
  `cout_maintenance` double unsigned DEFAULT NULL,
  `cout_global` double unsigned DEFAULT NULL,
  `cout_remplacement_courant` double unsigned DEFAULT NULL,
  `cout_maintenance_courant` double unsigned DEFAULT NULL,
  `cout_global_courant` double unsigned DEFAULT NULL,
  PRIMARY KEY (`id_resultat`),
  KEY `fk_resultat_scenario` (`id_scenario`),
  KEY `fk_resultat_piece` (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `resultats`
--

INSERT INTO `resultats` (`id_resultat`, `id_scenario`, `id_piece`, `cout_construction`, `cout_remplacement`, `cout_maintenance`, `cout_global`, `cout_remplacement_courant`, `cout_maintenance_courant`, `cout_global_courant`) VALUES
(1, 1, 1, 622, 1244, 248.8, 2114.8, 2252.88, 420.87, 3295.75),
(2, 2, 1, 320, 640, 480, 1440, 1159.04, 811.96, 2291),
(3, 3, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `scenarios`
--

CREATE TABLE IF NOT EXISTS `scenarios` (
  `id_scenario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_piece` int(11) unsigned NOT NULL,
  `nom_scenario` varchar(50) NOT NULL,
  `comment_scenario` text,
  PRIMARY KEY (`id_scenario`),
  KEY `fk_scenario_piece` (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `scenarios`
--

INSERT INTO `scenarios` (`id_scenario`, `id_piece`, `nom_scenario`, `comment_scenario`) VALUES
(1, 1, 'Chambre1_parquet', 'On utilise du parquet.'),
(2, 1, '26/03/2016', 'On utilise du linoleum.'),
(3, 2, '26/03/2016', 'On utilise du carrelage.'),
(4, 2, '26/03/2016', 'On utilise du parquet.'),
(5, 3, 'enduit', 'tout en enduit'),
(6, 3, 'enduit et brique', 'on utilise de l''enduit et de la brique (50:50)'),
(7, 3, 'brique', 'tout en brique');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_article_piece` FOREIGN KEY (`id_piece`) REFERENCES `pieces` (`id_piece`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_article_scenario` FOREIGN KEY (`id_scenario`) REFERENCES `scenarios` (`id_scenario`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `resultats`
--
ALTER TABLE `resultats`
  ADD CONSTRAINT `fk_resultat_piece` FOREIGN KEY (`id_piece`) REFERENCES `pieces` (`id_piece`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_resultat_scenario` FOREIGN KEY (`id_scenario`) REFERENCES `scenarios` (`id_scenario`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `scenarios`
--
ALTER TABLE `scenarios`
  ADD CONSTRAINT `fk_scenario_piece` FOREIGN KEY (`id_piece`) REFERENCES `pieces` (`id_piece`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
