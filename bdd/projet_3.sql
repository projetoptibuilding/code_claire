-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 26 Avril 2016 à 23:17
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projet_3`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id_article` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_scenario` int(11) unsigned DEFAULT NULL,
  `id_piece` int(11) unsigned DEFAULT NULL,
  `code_article` varchar(20) DEFAULT NULL,
  `MAJ_article` date DEFAULT NULL,
  `CUPI_article` varchar(3) DEFAULT NULL,
  `surface` float unsigned DEFAULT NULL,
  `poste` varchar(15) DEFAULT NULL,
  `type_materiau` varchar(25) DEFAULT NULL,
  `libelle` text,
  `fabricant` varchar(50) DEFAULT NULL,
  `unite` varchar(5) DEFAULT NULL,
  `prix_unitaire` float unsigned DEFAULT NULL,
  `duree_de_vie` float unsigned DEFAULT NULL,
  `taux_entretien` float unsigned DEFAULT NULL,
  `taux_remplacement` float unsigned DEFAULT NULL,
  PRIMARY KEY (`id_article`),
  KEY `fk_article_scenario` (`id_scenario`),
  KEY `fk_article_piece` (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `articles`
--

INSERT INTO `articles` (`id_article`, `id_scenario`, `id_piece`, `code_article`, `MAJ_article`, `CUPI_article`, `surface`, `poste`, `type_materiau`, `libelle`, `fabricant`, `unite`, `prix_unitaire`, `duree_de_vie`, `taux_entretien`, `taux_remplacement`) VALUES
(1, 1, 1, 'PAR-CHC-001', '2016-04-21', '', 30, 'sol', 'parquet', 'Parquet chêne contrecollé 2 plis épaisseur 9,5 mm', '', 'm2', 62.2, 20, 0.008, 1),
(2, 1, 1, 'EIT-END-001', '2016-04-21', '', 72, 'veture', 'enduit', 'Enduit monocouche OC 1 semi-allégé finition projeté rustique', 'Parexlanko', 'm2', 30, 50, 0.02, 0.5),
(3, 1, 1, 'COU-PVC-001', '2016-04-21', '', 30, 'toiture', 'PVC, Isolant', 'Panneaux sandwiches de couverture ép. mousse 100 mm, Up 0,24', 'Arval, modèle : Ondatherm 1040 TS', 'm2', 120, 50, 0.015, 1),
(4, 1, 1, 'EIT-PEM-001', '2016-04-21', '', 72, 'veture', 'peinture', 'Peinture mate finition C, phase aqueuse -support ciment', 'Levis', 'm2', 18.8, 15, 0, 1),
(5, 2, 1, 'CAR-GCM-001', '2016-04-26', '', 30, 'sol', 'carrelage', 'Carrelage grès cérame mat aspect grainé de 20 x 20 cm', 'Villeroy & Boch', 'm2', 52.4, 40, 0.005, 1),
(6, 2, 1, 'COU-PVC-001', '2016-04-21', '', 30, 'toiture', 'PVC, Isolant', 'Panneaux sandwiches de couverture ép. mousse 100 mm, Up 0,24', 'Arval, modèle : Ondatherm 1040 TS', 'm2', 120, 50, 0.015, 1),
(7, 2, 1, 'EIT-PEM-001', '2016-04-21', '', 72, 'veture', 'peinture', 'Peinture mate finition C, phase aqueuse -support ciment', 'Levis', 'm2', 18.8, 15, 0, 1),
(8, 2, 1, 'EIT-END-001', '2016-04-21', '', 72, 'veture', 'enduit', 'Enduit monocouche OC 1 semi-allégé finition projeté rustique', 'Parexlanko', 'm2', 30, 50, 0.02, 0.5),
(10, 3, 1, 'COU-PVC-001', '2016-04-21', '', 30, 'toiture', 'PVC, Isolant', 'Panneaux sandwiches de couverture ép. mousse 100 mm, Up 0,24', 'Arval, modèle : Ondatherm 1040 TS', 'm2', 120, 50, 0.015, 1),
(11, 3, 1, 'EIT-PEM-001', '2016-04-21', '', 72, 'veture', 'peinture', 'Peinture mate finition C, phase aqueuse -support ciment', 'Levis', 'm2', 18.8, 15, 0, 1),
(12, 3, 1, 'EIT-END-001', '2016-04-21', '', 72, 'veture', 'enduit', 'Enduit monocouche OC 1 semi-allégé finition projeté rustique', 'Parexlanko', 'm2', 30, 50, 0.02, 0.5),
(13, 3, 1, 'CAR-GCM-003', '2016-04-21', '', 30, 'sol', 'carrelage', 'Carrelage grès cérame mat aspect grainé de 30x30cm', '', 'm2', 38, 40, 0.003, 1),
(14, 4, 2, 'PAR-CHC-001', '2016-04-21', '', 6, 'sol', 'parquet', 'Parquet chêne contrecollé 2 plis épaisseur 9,5 mm', '', 'm2', 62.2, 20, 0.008, 1),
(15, 4, 2, 'COU-PVC-001', '2016-04-21', '', 27, 'toiture', 'PVC, Isolant', 'Panneaux sandwiches de couverture ép. mousse 100 mm, Up 0,24', 'Arval, modèle : Ondatherm 1040 TS', 'm2', 120, 50, 0.015, 1),
(16, 4, 2, 'EIT-END-001', '2016-04-21', '', 27, 'veture', 'enduit', 'Enduit monocouche OC 1 semi-allégé finition projeté rustique', 'Parexlanko', 'm2', 30, 50, 0.02, 0.5),
(17, 4, 2, 'EIT-PEM-001', '2016-04-21', '', 27, 'veture', 'peinture', 'Peinture mate finition C, phase aqueuse -support ciment', 'Levis', 'm2', 18.8, 15, 0, 1),
(19, 5, 2, 'COU-PVC-001', '2016-04-21', '', 27, 'toiture', 'PVC, Isolant', 'Panneaux sandwiches de couverture ép. mousse 100 mm, Up 0,24', 'Arval, modèle : Ondatherm 1040 TS', 'm2', 120, 50, 0.015, 1),
(20, 5, 2, 'EIT-END-001', '2016-04-21', '', 27, 'veture', 'enduit', 'Enduit monocouche OC 1 semi-allégé finition projeté rustique', 'Parexlanko', 'm2', 30, 50, 0.02, 0.5),
(21, 5, 2, 'EIT-PEM-001', '2016-04-21', '', 27, 'veture', 'peinture', 'Peinture mate finition C, phase aqueuse -support ciment', 'Levis', 'm2', 18.8, 15, 0, 1),
(22, 5, 2, 'CAR-GCM-001', '2016-04-26', '', 6, 'sol', 'carrelage', 'Carrelage grès cérame mat aspect grainé de 20 x 20 cm', 'Villeroy & Boch', 'm2', 52.4, 40, 0.005, 1);

-- --------------------------------------------------------

--
-- Structure de la table `informations`
--

CREATE TABLE IF NOT EXISTS `informations` (
  `id_information` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `taux_inflation` float DEFAULT NULL,
  `duree_exploitation` float unsigned DEFAULT NULL,
  `cout_ext_fixe` double DEFAULT NULL,
  `cout_ext_annuel` double DEFAULT NULL,
  `comment` text,
  `cout_externalite` double unsigned DEFAULT NULL,
  `cout_externalite_courant` double unsigned DEFAULT NULL,
  PRIMARY KEY (`id_information`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `informations`
--

INSERT INTO `informations` (`id_information`, `taux_inflation`, `duree_exploitation`, `cout_ext_fixe`, `cout_ext_annuel`, `comment`, `cout_externalite`, `cout_externalite_courant`) VALUES
(1, 2, 50, 60000, 10000, '', 560000, 905794.01);

-- --------------------------------------------------------

--
-- Structure de la table `pieces`
--

CREATE TABLE IF NOT EXISTS `pieces` (
  `id_piece` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom_piece` varchar(50) NOT NULL,
  `surface` float NOT NULL,
  `comment_piece` text,
  PRIMARY KEY (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pieces`
--

INSERT INTO `pieces` (`id_piece`, `nom_piece`, `surface`, `comment_piece`) VALUES
(1, 'Salle', 30, 'Pièce 6x5'),
(2, 'Entrée', 6, 'Entrée 1.5x4');

-- --------------------------------------------------------

--
-- Structure de la table `resultats`
--

CREATE TABLE IF NOT EXISTS `resultats` (
  `id_resultat` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_scenario` int(11) unsigned DEFAULT NULL,
  `id_piece` int(11) unsigned DEFAULT NULL,
  `cout_construction` double unsigned DEFAULT NULL,
  `cout_remplacement` double unsigned DEFAULT NULL,
  `cout_maintenance` double unsigned DEFAULT NULL,
  `cout_global` double unsigned DEFAULT NULL,
  `cout_remplacement_courant` double unsigned DEFAULT NULL,
  `cout_maintenance_courant` double unsigned DEFAULT NULL,
  `cout_global_courant` double unsigned DEFAULT NULL,
  PRIMARY KEY (`id_resultat`),
  KEY `fk_resultat_scenario` (`id_scenario`),
  KEY `fk_resultat_piece` (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `resultats`
--

INSERT INTO `resultats` (`id_resultat`, `id_scenario`, `id_piece`, `cout_construction`, `cout_remplacement`, `cout_maintenance`, `cout_global`, `cout_remplacement_courant`, `cout_maintenance_courant`, `cout_global_courant`) VALUES
(1, 1, 1, 8979.6, 7792.8, 5606.4, 22378.8, 14183.15, 9483.67, 32646.42),
(2, 2, 1, 8685.6, 5632.8, 5253, 19571.4, 10827.88, 8885.87, 28399.35),
(3, 3, 1, 8253.6, 5200.8, 5031, 18485.4, 9892.6, 8510.34, 26656.53),
(4, 4, 2, 4930.8, 2269.2, 3389.28, 10589.28, 4135.92, 5733.24, 14799.95),
(5, 5, 2, 4872, 1837.2, 3318.6, 10027.8, 3464.86, 5613.68, 13950.54);

-- --------------------------------------------------------

--
-- Structure de la table `scenarios`
--

CREATE TABLE IF NOT EXISTS `scenarios` (
  `id_scenario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_piece` int(11) unsigned NOT NULL,
  `nom_scenario` varchar(50) NOT NULL,
  `comment_scenario` text,
  PRIMARY KEY (`id_scenario`),
  KEY `fk_scenario_piece` (`id_piece`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `scenarios`
--

INSERT INTO `scenarios` (`id_scenario`, `id_piece`, `nom_scenario`, `comment_scenario`) VALUES
(1, 1, 'Test 1', 'Sol parquet'),
(2, 1, 'Test 2', 'Sol carrelage'),
(3, 1, 'Test 2_Copie', 'Sol carrelage (2éme type)'),
(4, 2, 'Test 1', 'Entrée parquet\r\n'),
(5, 2, 'Test 1_Copie', 'Entrée carrelage');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_article_piece` FOREIGN KEY (`id_piece`) REFERENCES `pieces` (`id_piece`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_article_scenario` FOREIGN KEY (`id_scenario`) REFERENCES `scenarios` (`id_scenario`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `resultats`
--
ALTER TABLE `resultats`
  ADD CONSTRAINT `fk_resultat_piece` FOREIGN KEY (`id_piece`) REFERENCES `pieces` (`id_piece`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_resultat_scenario` FOREIGN KEY (`id_scenario`) REFERENCES `scenarios` (`id_scenario`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `scenarios`
--
ALTER TABLE `scenarios`
  ADD CONSTRAINT `fk_scenario_piece` FOREIGN KEY (`id_piece`) REFERENCES `pieces` (`id_piece`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
