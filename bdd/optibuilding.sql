-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mar 26 Avril 2016 à 23:16
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `optibuilding`
--

-- --------------------------------------------------------

--
-- Structure de la table `materiaux`
--

CREATE TABLE IF NOT EXISTS `materiaux` (
  `id_mat` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identifiant du matériau',
  `code_mat` varchar(255) NOT NULL COMMENT 'code matériau, codification choisit par le groupe projet',
  `MAJ_mat` date NOT NULL,
  `CUPI_mat` varchar(3) DEFAULT NULL,
  `poste_mat` varchar(15) NOT NULL COMMENT 'le domaine où est utilisé le matériau (toiture, sol...)',
  `type_mat` varchar(255) NOT NULL COMMENT 'type du matériau',
  `libelle_mat` text NOT NULL COMMENT 'nom et caractéristiques techniques',
  `fabricant_mat` varchar(255) DEFAULT NULL COMMENT 'nom du fabricant',
  `unite_mat` varchar(5) NOT NULL COMMENT 'unité',
  `prix_unitaire_mat` float NOT NULL COMMENT 'prix unitaire en Euro',
  `duree_de_vie_mat` float NOT NULL COMMENT 'durée de vie en année',
  `taux_entretien_mat` float DEFAULT NULL COMMENT 'taux annuel d''entretien (par rapport au coût global)',
  `taux_remplacement_mat` float DEFAULT NULL COMMENT 'taux de remplacement (par rapport au coût global)',
  PRIMARY KEY (`id_mat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Cette table rassemble tous les matériaux utilisés' AUTO_INCREMENT=53 ;

--
-- Contenu de la table `materiaux`
--

INSERT INTO `materiaux` (`id_mat`, `code_mat`, `MAJ_mat`, `CUPI_mat`, `poste_mat`, `type_mat`, `libelle_mat`, `fabricant_mat`, `unite_mat`, `prix_unitaire_mat`, `duree_de_vie_mat`, `taux_entretien_mat`, `taux_remplacement_mat`) VALUES
(1, 'CAR-GCM-001', '2016-04-26', '', 'sol', 'carrelage', 'Carrelage grès cérame mat aspect grainé de 20 x 20 cm', 'Villeroy & Boch', 'm2', 52.4, 40, 0.005, 1),
(2, 'CAR-GEM-002', '2016-04-21', '', 'sol', 'carrelage', 'Carrelage grès émaillé mat aspect uni de 20 x 20 cm', '', 'm2', 51.7, 30, 0.0075, 1.0832),
(3, 'CAR-GCM-002', '2016-04-21', '', 'sol', 'carrelage', 'Carrelage grès cérame mat aspect granité de 40 x 40 cm', 'Marrazi', 'm2', 52.1, 40, 0.005, 1.0749),
(4, 'SSO-PVC-001', '2016-04-21', '', 'sol', 'pvc', 'Lés PVC sur mousse, acoustique 19 dB', 'Tarkett Bâtiment', 'm2', 32, 20, 0.03, 1),
(5, 'SSO-PVC-002', '2016-04-21', '', 'sol', 'pvc', 'Dalles PVC sur mousse, acoustique 19 dB', 'Tarkett Bâtiment', 'm2', 33.5, 20, 0.03, 1),
(6, 'SSO-CAO-001', '2016-04-21', '', 'sol', 'caoutchouc', 'Rev&ecirc;tement caoutchouc &agrave; pastilles rondes classiques, uni', 'Nora', 'm2', 46.6, 50, 0.01, 1),
(7, 'PAR-CHM-001', '2016-03-21', '', 'sol', 'parquet', 'Parquet chêne massif épaisseur 10 mm\r\n', '', 'm2', 38.9, 20, 0.013, 1),
(8, 'PAR-CHC-001', '2016-04-21', '', 'sol', 'parquet', 'Parquet chêne contrecollé 2 plis épaisseur 9,5 mm', '', 'm2', 62.2, 20, 0.008, 1),
(9, 'PAR-CHC-002', '2016-04-21', '', 'sol', 'parquet', 'Parquet chêne contrecollé, classe 31, ép. 15 mm', '', 'm2', 54.4, 20, 0, 1),
(10, 'SSO-TEX-001', '2016-04-21', '', 'sol', 'textile', 'Lés textiles aiguilletés plats', 'Tecsom Bâtiment', 'm2', 21.5, 10, 0.05, 1),
(11, 'SSO-MOQ-001', '2016-04-21', '', 'sol', 'textile', 'Moquette tuftée à velours coupé sur dossier synthétique U2S', 'Balsan', 'm2', 20, 10, 0.05, 1),
(12, 'SSO-TEX-002', '2016-04-21', '', 'sol', 'textile', 'Revêtement textile floqué U3S P3 en dalles velours imprimé', 'Bonar Floor', 'm2', 36, 15, 0.05, 1),
(13, 'EIT-END-001', '2016-04-21', '', 'veture', 'enduit', 'Enduit monocouche OC 1 semi-allégé finition projeté rustique', 'Parexlanko', 'm2', 30, 50, 0.02, 0.5),
(14, 'EIT-EPP-001', '2016-04-21', '', 'veture', 'enduit', 'Enduit pour parement peint', 'Parexlanko', 'm2', 31, 20, 0.02, 0.5),
(15, 'EIT-EPM-001', '2016-04-21', '', 'veture', 'enduit', 'Enduit de parement minéral CR finition gratté', 'Parexlanko', 'm2', 36.9, 50, 0.015, 0.4),
(16, 'EIT-EPM-002', '2016-04-21', '', 'veture', 'enduit', 'Enduit de parement minéral CR finition rustique écrasé', 'Parexlanko', 'm2', 36.9, 50, 0.015, 0.4),
(17, 'EIT-PEM-001', '2016-04-21', '', 'veture', 'peinture', 'Peinture mate finition C, phase aqueuse -support ciment', 'Levis', 'm2', 18.8, 15, 0, 1),
(18, 'EIT-PES-001', '2016-04-21', '', 'veture', 'peinture', 'Peinture satinée finition C, phase aqueuse -support brique', '', 'm2', 16.5, 15, 0, 1),
(19, 'RFA-BRI-001', '2016-04-21', '', 'veture', 'brique', 'Revêtement mural extérieur briques pleines 22 x 10,5 x 5', '', 'm2', 83, 100, 0.01, 1),
(20, 'RFA-BRI-002', '2016-04-21', '', 'veture', 'brique', 'Revêtement mural extérieur plaquettes briques 22 x 1,5 x 5', '', 'm2', 58, 100, 0.01, 1),
(21, 'RFA-BRI-003', '2016-04-21', '', 'veture', 'brique', 'Revêtement mural extérieur mulots briques 22 x 5 x 5', '', 'm2', 72, 100, 0.005, 1),
(22, 'BAR-MOE-001', '2016-04-21', '', 'veture', 'moellon', 'Revêtement mural en moellons', '', 'm2', 75, 100, 0.005, 1),
(23, 'BAR-PVC-001', '2016-04-21', '', 'veture', 'bardage_pvc', 'Bardage lambris PVC', '', 'm2', 60.5, 40, 0, 1),
(24, 'COU-TUI-001', '2016-04-21', '', 'toiture', 'tuile', 'Couverture tuiles canal fixation par crochetage', '', 'm2', 36, 100, 0.015, 1),
(25, 'COU-TUI-002', '2016-04-21', '', 'toiture', 'tuile', 'Tuiles à emboîtement ou à glissement type Romane', '', 'm2', 22.7, 100, 0.015, 1),
(26, 'COU-TUI-003', '2016-04-21', '', 'toiture', 'tuile', 'Tuiles béton à glissement et à emboîtement', '', 'm2', 21, 100, 0.015, 1),
(27, 'COU-ARD-001', '2016-04-21', '', 'toiture', 'ardoise', 'Ardoises fibres-ciment mod&egrave;le 40 x 24', '', 'm2', 30, 60, 0.03, 1),
(28, 'BAR-BOI-001', '2016-04-21', '', 'veture', 'bardage', 'Bardage bois classe 4, raboté , 22x135mm', '', 'm2', 114.38, 50, 0.05, 1),
(29, 'CAR-GCM-003', '2016-04-21', '', 'sol', 'carrelage', 'Carrelage grès cérame mat aspect grainé de 30x30cm', '', 'm2', 38, 40, 0.003, 1),
(30, 'SSO-PVC-003', '2016-04-21', '', 'sol', 'pvc', 'Lès PVC de type vinyle sur mousse, acoustique 19dB', '', 'm2', 20, 20, 0.03, 1),
(31, 'COU-POL-001', '2016-04-21', '', 'toiture', 'polyuréthane', 'Isolation dalle béton en polyuréthane R 4,35 ép. 100mm', '', 'm2', 61.02, 50, 0, 1),
(32, 'EIT-END-002', '2016-04-21', '', 'veture', 'enduit', 'Enduit des parements extérieurs', '', 'm2', 16.44, 10, 0, 0.3252),
(33, 'RFA-BRI-004', '2016-04-21', '', 'veture', 'brique', 'Revêtement mural extérieur briques pleines 22x10,5x5', '', 'm2', 117, 100, 0.02, 1),
(34, 'EIT-EPS-001', '2016-04-21', '', 'veture', 'enduit', 'Enduit pour parements scellés', '', 'm2', 34.07, 50, 0, 1),
(35, 'COU-TUI-004', '2016-04-21', '', 'toiture', 'tuile', 'Couverture tuiles canal fixation par mortier', '', 'm2', 38, 100, 0.015, 1),
(39, 'COU-TUI-005', '2016-04-21', '', 'toiture', 'tuile', 'Couverture tuiles canal dotée d''un système de blocage', '', 'm2', 36, 100, 0.015, 1),
(42, 'COU-TUI-006', '2016-04-21', '', 'toiture', 'tuile', 'Tuiles à emboîtement ou à glissement type Romane', '', 'm2', 22.7, 100, 0.015, 1.01),
(45, 'COU-TUI-007', '2016-04-21', '', 'toiture', 'tuile', 'Tuiles émaillées à emboîtement ou à glissement type Romane', '', 'm2', 34.2, 100, 0.015, 1.02),
(46, 'COU-ARD-002', '2016-04-21', '', 'toiture', 'tuile', 'Ardoises de dimensions 300 x 200 mm, ép. 3,5 mm', '', 'm2', 68.2, 150, 0.015, 1),
(48, 'COU-ARD-003', '2016-04-21', '', 'toiture', 'tuile', 'Ardoises de dimensions 400 x 220 mm, ép. 5 mm', '', 'm2', 110, 150, 0.01, 1),
(50, 'COU-PVC-001', '2016-04-21', '', 'toiture', 'PVC, Isolant', 'Panneaux sandwiches de couverture ép. mousse 100 mm, Up 0,24', 'Arval, modèle : Ondatherm 1040 TS', 'm2', 120, 50, 0.015, 1),
(51, 'COU-ZIN-001', '2016-04-21', '', 'toiture', 'Zinc', 'Couverture zinc ''à agrafure joint debout''', '', 'm2', 49, 100, 0.01, 1),
(52, 'COU-ZIN-002', '2016-04-21', '', 'toiture', 'Zinc', 'Couverture zinc à tasseaux ''à double agrafure''', '', 'm2', 65, 100, 0.01, 1);

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE IF NOT EXISTS `projet` (
  `id_project` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_delivery` date NOT NULL,
  `date_modification` datetime NOT NULL,
  `id_author` int(11) NOT NULL,
  `adress` varchar(256) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `comment` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id_project`),
  KEY `id_author` (`id_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `projet`
--

INSERT INTO `projet` (`id_project`, `name`, `date_creation`, `date_delivery`, `date_modification`, `id_author`, `adress`, `zip_code`, `comment`) VALUES
(2, 'Centrale Lille', '2016-04-26 20:16:35', '2016-05-04', '2016-04-26 22:26:39', 9, 'Villeneuve-d''Ascq', 59650, 'Création d''un FABLAB'),
(3, 'Norevie', '2016-04-26 20:37:53', '2017-04-27', '2016-04-26 23:00:18', 12, 'Douai', 59500, 'Projet test');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'clé primaire, identifiant numérique auto incrémenté',
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL COMMENT 'pseudo',
  `passe` varchar(20) NOT NULL COMMENT 'mot de passe',
  `admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'indique si l''utilisateur est un administrateur',
  `valide` tinyint(1) NOT NULL DEFAULT '0',
  `connecte` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'indique si l''utilisateur est connecte',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `email`, `passe`, `admin`, `valide`, `connecte`) VALUES
(9, 'BRESSE', 'Claire', 'clairebresse13@gmail.com', 'swaro', 1, 1, 0),
(10, 'BERTRAND', 'Pierre', 'pierre.bertrand@gmail.com', 'clp', 0, 1, 0),
(11, 'BALLET', 'Romain', 'romain.ballet@gmail.com', 'plongee', 0, 1, 0),
(12, 'FOUCHIER', 'Marin', 'marin.fouchier@gmail.com', 'ctglobal', 0, 1, 0),
(13, 'CAMIAT', 'Fanny', 'fanny.camiat@gmail.com', 'metz', 0, 0, 0),
(16, 'TANIERE', 'Joris', 'joris.taniere@gmail.com', 'excel', 0, 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
