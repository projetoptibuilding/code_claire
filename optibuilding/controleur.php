<?php
session_start();

	include_once "libs/maLibUtils.php";
	include_once "libs/maLibSQL.pdo.php";
	include_once "libs/maLibSecurisation.php"; 
	include_once "libs/modele.php"; 

	$addArgs = "";
		
	if ($action = valider("action"))
	{
		ob_start ();
		echo "Action = '$action' <br />";
		// ATTENTION : le codage des caractères peut poser PB si on utilise des actions comportant des accents... 

		// Un paramètre action a été soumis, on fait le boulot...
		switch($action)
		{
			case 'Valider' :
				if ($idUser = valider("idUser"))
				{
					validerUtilisateur($idUser); 
				}
				$addArgs = "?view=users&idUserPrecedent=$idUser";
			break;
		
			case 'Supprimer' :
				if ($idUser = valider("idUser"))
				{
					supprimerUtilisateur($idUser);
				}
				$addArgs = "?view=users&idUserPrecedent=$idUser";
			break;
		
			case 'Modifier' :
				if ($nom = valider("nom")
					and $prenom = valider("prenom")
					and $login = valider ("login")
					and $passe = valider("passe")
					and $passe_conf = valider("passe_conf"))
				{
					if ($passe != $passe_conf)
						$addArgs = "?view=compte&msg=" . urlencode("La confirmation du mot de passe ne correspond pas au mot de passe.");
					else
						modifierCompte($nom,$prenom,$login,$passe);
						$addArgs = "?view=compte&msg=" . urlencode("La modification a été effectuée.");
				}
			break;
		
			case 'ajouter_mat' :
				$addArgs = "?view=insertion_mat";
			break;
		
			case 'modif_ret_mat' :
				$addArgs = "?view=modif_ret_mat";
			break;
		
			case 'creer_mat' :
				if ($code_mat = valider("code_mat")
					and $poste = valider("poste")
					and $type_materiau = valider("type_materiau")
					and $libelle = valider("libelle")
					and $prix_unitaire = valider("prix_unitaire")
					and $unite = valider("unite")
					and $duree_de_vie = valider("duree_de_vie"))
				{
					$cupi = protect("cupi");
					$fabricant = protect("fabricant");
					$taux_entretien = protect("taux_entretien");
					$taux_remplacement = protect("taux_remplacement");
					ajouterMat($code_mat,$cupi,$poste,$type_materiau,$libelle,$fabricant,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement);
					$addArgs = "?view=materiaux";
				}
			break;
		
			case 'retour_table' :
				$addArgs = "?view=materiaux";
			break;
			
			case 'modifier_mat' :
				if ($code_mat = valider("code_mat")
					and $poste = valider("poste")
					and $type_materiau = valider("type_materiau")
					and $libelle = valider("libelle")
					and $prix_unitaire = valider("prix_unitaire")
					and $unite = valider("unite")
					and $duree_de_vie = valider("duree_de_vie"))
				{
					$cupi = protect("cupi");
					$fabricant = protect("fabricant");
					$taux_entretien = protect("taux_entretien");
					$taux_remplacement = protect("taux_remplacement");
					modifierMat($code_mat,$cupi,$poste,$type_materiau,$libelle,$fabricant,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement);
					$addArgs = "?view=materiaux";
				}
			break;
		
			case "importer_mat" :
				if ($id_mat = valider("id_mat"))
				{
					$quantite = protect("quantite");
					$id_project=$_SESSION['projet'];
					$bdd='projet_'.$id_project;
					importerMat($bdd,$id_mat,$quantite);
					majProjet($id_project);
					$id_piece=$_SESSION['piece'];
					$id_sce=$_SESSION['scenario'];
					$addArgs = "?view=table_articles&projet=$id_project&piece=$id_piece&sce=$id_sce";
				}
			break;
		
			case 'annuler' :
				$addArgs = "?view=modif_ret_mat";
			break;
		
			case 'nouveau_projet' :
				$addArgs = "?view=nouveau_projet";
			break;
		
			case 'creer_projet' :
				if ($name = valider("name")
					and $date_delivery = valider("date_delivery")
					and $adress = valider("adress")
					and $zip_code = valider("zip_code"))
				{
					$comment = protect("comment");
					creerProjet($name,$date_delivery,$adress,$zip_code,$comment);
					$addArgs = "?view=creer_projet";
				}
			break;
		
			case "synthese_projet" :
				$id_projet=$_SESSION['projet'];
				$addArgs = "?view=comparaison&projet=$id_projet";
			break;
		
			case "modif_projet" :
				if ($id_projet = valider("id_projet"))
					$addArgs = "?view=modif_projet&projet=$id_projet";
			break;
		
			case 'modifier_projet' :
				if ($id_projet = valider("id_projet")
					and $name = valider("name")
					and $date_delivery = valider("date_delivery")
					and $adress = valider("adress")
					and $zip_code = valider("zip_code"))
				{
					$comment = protect("comment");
					modifierProjet($id_projet,$name,$date_delivery,$adress,$zip_code,$comment);
					$addArgs = "?view=projets";
				}
			break;
		
			case 'suppr_projet' :
				if ($id_projet = valider("id_projet"))
				{
					supprProjet($id_projet);
					$addArgs = "?view=projets";
				}
			break;	
		
			case 'nouvelle_piece' :
				$id_project=$_SESSION['projet'];
				$addArgs = "?view=nouvelle_piece&projet=$id_project";
			break;
		
			case 'creer_piece' :
				if ($nom_piece = valider("nom_piece")
					and $surface = valider("surface"))
				{
					$comment = protect("comment");
					$id_project=$_SESSION['projet'];
					$bdd='projet_'.$id_project;
					creerPiece($bdd,$nom_piece,$surface,$comment);
					$addArgs = "?view=pieces&projet=$id_project";
				}
			break;
		
			case "modif_piece" :
				if ($id_piece = valider("id_piece"))
				{
					$id_projet=$_SESSION['projet'];
					$addArgs = "?view=modif_piece&projet=$id_projet&piece=$id_piece";
				}
			break;
		
			case 'modifier_piece' :
				if ($id_piece = valider("id_piece")
					and $nom_piece = valider("nom_piece")
					and $surface = valider("surface"))
				{
					$comment = protect("comment");
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					modifierPiece($bdd,$id_piece,$nom_piece,$surface,$comment);
					majProjet($id_projet);
					$addArgs = "?view=pieces&projet=$id_projet";
				}
			break;
		
			case "retirer_piece" :
				if($id_piece = valider("id_piece"))
				{
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					supprPiece($bdd,$id_piece);
					majProjet($id_projet);
					$addArgs = "?view=pieces&projet=$id_projet";
				}
			break;
		
			case 'retour_pieces' :
				$id_project=$_SESSION['projet'];
				$addArgs = "?view=pieces&projet=$id_project";
			break;
		
			case 'nouveau_scenario' :
				$id_project=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$addArgs = "?view=nouveau_scenario&projet=$id_project&piece=$id_piece";
			break;
		
			case 'creer_scenario' :
				if ($name = valider("name"))
				{
					$comment = protect("comment");
					$id_project=$_SESSION['projet'];
					$bdd='projet_'.$id_project;
					$id_piece=$_SESSION['piece'];
					creerScenario($bdd,$id_piece,$name,$comment);
					$addArgs = "?view=gestion_scenario&projet=$id_project&piece=$id_piece";
				}
			break;
		
			case 'copier_sce' :
				if ($id_scenario = valider("id_scenario")
					and $nom_scenario = valider("nom_scenario"))
				{
					$comment_scenario = protect("comment_scenario");
					$id_projet=$_SESSION['projet'];
					$projet='projet_'.$id_projet;
					$id_piece=$_SESSION['piece'];
					copierScenario($projet,$id_scenario,$id_piece,$nom_scenario,$comment_scenario);
					majProjet($id_projet);
					$addArgs = "?view=gestion_scenario&projet=$id_projet&piece=$id_piece";
				}
			break;
		
			case 'copier_maj' :
				if ($id_scenario = valider("id_scenario")
					and $nom_scenario = valider("nom_scenario"))
				{
					$comment_scenario = protect("comment_scenario");
					$id_projet=$_SESSION['projet'];
					$projet='projet_'.$id_projet;
					$id_piece=$_SESSION['piece'];
					copierSceMaj($projet,$id_scenario,$id_piece,$nom_scenario,$comment_scenario);
					majProjet($id_projet);
					$addArgs = "?view=gestion_scenario&projet=$id_projet&piece=$id_piece";
				}
			break;
		
			case "modif_sce" :
				if ($id_scenario = valider("id_scenario"))
				{
					$id_projet=$_SESSION['projet'];
					$id_piece=$_SESSION['piece'];
					$addArgs = "?view=modif_sce&projet=$id_projet&piece=$id_piece&sce=$id_scenario";
				}
			break;
		
			case 'modifier_sce' :
				if ($id_scenario = valider("id_scenario")
					and $name = valider("name"))
				{
					$comment = protect("comment");
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					$id_piece=$_SESSION['piece'];
					modifierSce($bdd,$id_scenario,$name,$comment);
					majProjet($id_projet);
					$addArgs = "?view=gestion_scenario&projet=$id_projet&piece=$id_piece";
				}
			break;
		
			case "retirer_sce" :
				if($id_scenario = valider("id_scenario"))
				{
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					$id_piece=$_SESSION['piece'];
					supprSce($bdd,$id_scenario);
					majProjet($id_projet);
					$addArgs = "?view=gestion_scenario&projet=$id_projet&piece=$id_piece";
				}
			break;
				
			case "retour_sce" :
				$id_project=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$addArgs = "?view=gestion_scenario&projet=$id_project&piece=$id_piece";
			break;	
			
			case "retour_sce_id" :
				$id_project=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$id_sce=$_SESSION['scenario'];
				$addArgs = "?view=scenario&projet=$id_project&piece=$id_piece&sce=$id_sce";	
			break;
		
			case "table_articles" :
				$id_project=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$id_sce=$_SESSION['scenario'];
				$addArgs = "?view=table_articles&projet=$id_project&piece=$id_piece&sce=$id_sce";	
			break;
		
			case "modif_art" :
				if ($id_article = valider("id_article"))
				{
					$id_project=$_SESSION['projet'];
					$id_piece=$_SESSION['piece'];
					$id_sce=$_SESSION['scenario'];
					$addArgs = "?view=modification_article&projet=$id_project&piece=$id_piece&sce=$id_sce&art=$id_article";
				}	
			break;
		
			case "retirer_art" :
				if ($id_article = valider("id_article"))
				{
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					$id_piece=$_SESSION['piece'];
					$id_sce=$_SESSION['scenario'];
					supprimerArt($bdd,$id_article);
					majProjet($id_projet);
					$addArgs = "?view=table_articles&projet=$id_projet&piece=$id_piece&sce=$id_sce";
				}	
			break;
		
			case "importer_art" :
				$id_project=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$id_sce=$_SESSION['scenario'];
				$addArgs = "?view=importer_article&projet=$id_project&piece=$id_piece&sce=$id_sce";	
			break;
		
			case "nouvel_art" :
				$id_project=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$id_sce=$_SESSION['scenario'];
				$addArgs = "?view=nouvel_article&projet=$id_project&piece=$id_piece&sce=$id_sce";	
			break;
		
			case "creer_art" :
				if ($code_mat = valider("code_mat")
					and $poste = valider("poste")
					and $type_materiau = valider("type_materiau")
					and $libelle = valider("libelle")
					and $prix_unitaire = valider("prix_unitaire")
					and $unite = valider("unite")
					and $duree_de_vie = valider("duree_de_vie"))
				{
					$cupi = protect("cupi");
					$fabricant = protect("fabricant");
					$surface = protect("surface");
					$taux_entretien = protect("taux_entretien");
					$taux_remplacement = protect("taux_remplacement");
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					ajouterArt($bdd,$code_mat,$cupi,$poste,$type_materiau,$libelle,$fabricant,$surface,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement);
					majProjet($id_projet);
					$id_piece=$_SESSION['piece'];
					$id_sce=$_SESSION['scenario'];
					$addArgs = "?view=table_articles&projet=$id_projet&piece=$id_piece&sce=$id_sce";
				}
			break;
		
			case "modifier_article" :
				if ($id_article = valider("id_article")
					and $code_article = valider("code_article")
					and $poste = valider("poste")
					and $type_materiau = valider("type_materiau")
					and $libelle = valider("libelle")
					and $prix_unitaire = valider("prix_unitaire")
					and $unite = valider("unite")
					and $duree_de_vie = valider("duree_de_vie"))
				{
					$cupi = protect("cupi");
					$fabricant = protect("fabricant");
					$surface = protect("surface");
					$taux_entretien = protect("taux_entretien");
					$taux_remplacement = protect("taux_remplacement");
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					modifierArt($bdd,$id_article,$code_article,$cupi,$poste,$type_materiau,$libelle,$fabricant,$surface,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement);
					majProjet($id_projet);
					$id_piece=$_SESSION['piece'];
					$id_sce=$_SESSION['scenario'];
					$addArgs = "?view=table_articles&projet=$id_projet&piece=$id_piece&sce=$id_sce";
				}
			break;	
		
			case "verif_maj" :
				$id_project=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$id_sce=$_SESSION['scenario'];
				$addArgs = "?view=maj&projet=$id_project&piece=$id_piece&sce=$id_sce";	
			break;
		
			case "maj_art" :
				if ($id_article = valider("id_article")
					and $MAJ_article = valider("MAJ_article")
					and $poste = valider("poste")
					and $type_materiau = valider("type_materiau")
					and $libelle = valider("libelle")
					and $prix_unitaire = valider("prix_unitaire")
					and $unite = valider("unite")
					and $duree_de_vie = valider("duree_de_vie"))
				{
					$CUPI_article = protect("CUPI_article");
					$fabricant = protect("fabricant");
					$taux_entretien = protect("taux_entretien");
					$taux_remplacement = protect("taux_remplacement");
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					majArt($bdd,$id_article,$MAJ_article,$CUPI_article,$poste,$type_materiau,$libelle,$fabricant,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement);
					majProjet($id_projet);
					$id_piece=$_SESSION['piece'];
					$id_sce=$_SESSION['scenario'];
					$addArgs = "?view=table_articles&projet=$id_projet&piece=$id_piece&sce=$id_sce";
				}
			break;
		
			case "infos_projet" :
				$id_project=$_SESSION['projet'];
				if ($scenario = valider("scenario"))
				{
					$id_piece=$_SESSION['piece'];
					$addArgs = "?view=infos_projet&projet=$id_project&piece=$id_piece&sce=$scenario";
				}
				else $addArgs = "?view=infos_projet&projet=$id_project";
			break;
				
			case "sauvegarde_infos" :
				if($duree_exploitation = valider("duree_exploitation"))
				{
					$id_projet=$_SESSION['projet'];
					$bdd='projet_'.$id_projet;
					$taux_inflation = protect("taux_inflation");
					$cout_ext_fixe = protect("cout_ext_fixe");
					$cout_ext_annuel = protect("cout_ext_annuel");
					$comment = protect("comment");
					$cout_externalite = protect("cout_externalite");
					$cout_externalite_courant = protect("cout_externalite_courant");
					saveInfos($bdd,$taux_inflation,$duree_exploitation,$cout_ext_fixe,$cout_ext_annuel,$comment,$cout_externalite,$cout_externalite_courant);
					majProjet($id_projet);
					if ($id_scenario = valider("id_scenario"))
					{
						$id_piece=$_SESSION['piece'];
						$id_sce=$_SESSION['scenario'];
						$addArgs = "?view=scenario&projet=$id_projet&piece=$id_piece&sce=$id_sce";
					}
					else $addArgs = "?view=pieces&projet=$id_projet";
				}
			break;
		
			case "sauvegarde_resultats" :
				if ($projet = valider("projet")
					and $piece = valider("piece")
					and $scenario = valider("scenario"))
				{
					$cout_construction = protect("cout_construction");
					$cout_remplacement = protect("cout_remplacement");
					$cout_maintenance = protect("cout_maintenance");
					$cout_global = protect("cout_global");
					$cout_remplacement_courant = protect("cout_remplacement_courant");
					$cout_maintenance_courant = protect("cout_maintenance_courant");
					$cout_global_courant = protect("cout_global_courant");
					saveResultats($projet,$piece,$scenario,$cout_construction,$cout_remplacement,$cout_maintenance,$cout_global,$cout_remplacement_courant,$cout_maintenance_courant,$cout_global_courant);
					majProjet($projet);
					$addArgs = "?view=scenario&projet=$projet&piece=$piece&sce=$scenario";
				}
			break;
		
			case "analyse" :
				$id_projet=$_SESSION['projet'];
				$id_piece=$_SESSION['piece'];
				$id_sce=$_SESSION['scenario'];
				$addArgs = "?view=analyse&projet=$id_projet&piece=$id_piece&sce=$id_sce";
			break;
			
			// Connexion //////////////////////////////////////////////////
			case 'Connexion' :
				// On verifie la presence des champs login et passe
				if ($login = valider("login"))
				if ($passe = valider("passe"))
					{
						// On verifie l'utilisateur, 
						// et on crée des variables de session si tout est OK
						// Cf. maLibSecurisation
						if (!verifMdp($login,$passe))
							$addArgs = "?view=login&msg=" . urlencode("Mot de passe incorrect.");
						else
						{
							verifUser($login,$passe);
							connecteUser($login);
						}
					}
					
				// On redirigera vers la page index automatiquement
			break;

			case 'Logout' :
				
					deconnecteUser($_SESSION["idUser"]);
					session_destroy();	
				
			break;

			case 'creer_compte' :
				if ($nom = valider("nom") and $prenom = valider("prenom") and $login = valider("login") and $passe = valider("passe") and $passe_conf = valider("passe_conf"))
				{
					if (!verifLogin($login))
						$addArgs = "?view=login&msg=" . urlencode("Votre login est d&eacutej&agrave utilis&eacute.");
					else if ($passe != $passe_conf)
						$addArgs = "?view=login&msg=" . urlencode("La confirmation du mot de passe ne correspond pas au mot de passe.");
					else
					{
						ajouterUser($nom,$prenom,$login,$passe);
						verifUser($login,$passe);
						connecteUser($login);
					}
				}
			break;
		}

	}

	// On redirige toujours vers la page index, mais on ne connait pas le répertoire de base
	// On l'extrait donc du chemin du script courant : $_SERVER["PHP_SELF"]

	$urlBase = dirname($_SERVER["PHP_SELF"]) . "/index.php";
	// On redirige vers la page index avec les bons arguments

	header("Location:" . $urlBase . $addArgs);

	// On écrit seulement après cette entête
	ob_end_flush();
	
?>