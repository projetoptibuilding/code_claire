<?php
session_start();

/*
Cette page génère les différentes vues de l'application en utilisant des templates situés dans le répertoire "templates". Un template ou 'gabarit' est un fichier php qui génère une partie de la structure XHTML d'une page. 

La vue à afficher dans la page index est définie par le paramètre "view" qui doit être placé dans la chaîne de requête. En fonction de la valeur de ce paramètre, on doit vérifier que l'on a suffisamment de données pour inclure le template nécessaire, puis on appelle le template à l'aide de la fonction include
*/

include_once "libs/maLibUtils.php";
include_once "libs/maLibBootstrap.php";

// on récupère le paramètre view éventuel 
$view = valider("view"); 

// S'il est vide, on charge la vue accueil par défaut
if (!$view) $view = "accueil";

// NB : il faut que view soit défini avant d'appeler l'entête

// Dans tous les cas, on affiche l'entete, 
// qui contient les balises de structure de la page, le logo, etc. 
// Le formulaire de recherche ainsi que le lien de connexion 
// si l'utilisateur n'est pas connecté 

include("templates/header.php");

// En fonction de la vue à afficher, on appelle tel ou tel template
switch($view)
{		
	case "accueil" : 
		include("templates/accueil.php");
	break;

	case "analyse" :
		if (!valider("connecte","SESSION")) header("Location:index.php");
		else include("templates/calcul/$view.php");
	break;

	case "fiche_article" :
	case "insertion_mat" :
	case "materiaux" :
	case "modif_ret_mat" :
	case "modification_mat" :
	case "retirer_mat" :
		if (!valider("connecte","SESSION")) header("Location:index.php");
		else include("templates/materiaux/$view.php");
	break;

	case "comparaison" :
	case "creer_projet" :
	case "gestion_scenario" :
	case "importer_article" :
	case "infos_projet" :
	case "maj" :
	case "modif_piece" :	
	case "modif_projet" :
	case "modif_sce" :
	case "modification_article" :
	case "nouveau_projet" :
	case "nouveau_scenario" :
	case "nouvel_article" :
	case "nouvelle_piece" :
	case "pieces" :
	case "projets" :
	case "scenario" :
	case "table_articles" :
		if (!valider("connecte","SESSION")) header("Location:index.php");
		else include("templates/projets/$view.php");
	break;

	case "login" :
		include("templates/utilisateurs/$view.php");
	break;

	case "compte" :
	case "users" :
		if (!valider("connecte","SESSION")) header("Location:index.php");
		else include("templates/utilisateurs/$view.php");
	break;

	default :
		if (file_exists("templates/$view.php"))
			include("templates/$view.php");
	break;
}

// Dans tous les cas, on affiche le pied de page
// Qui contient les coordonnées de la personne si elle est connectée
include("templates/footer.php");
	
?>