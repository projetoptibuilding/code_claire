<?php

// inclure ici la librairie faciliant les requêtes SQL
include_once("maLibSQL.pdo.php");
include_once ("config.php");

function selectChamp($tabData,$nomChamp,$champReference,$nomReference)
{
	$SQL="SELECT $nomChamp FROM $tabData where $champReference='$nomReference'";
	return SQLGetChamp($SQL);
}

function listerChamp($tabData,$nomChamp,$champReference,$nomReference)
{
	$SQL="SELECT $nomChamp FROM $tabData where $champReference='$nomReference'";
	return parcoursRs(SQLSelect($SQL));
}

//$str est la chaîne de caractères et $nb le nombre de caractères maximum à afficher.
function tronque($str, $nb = 150) 
{
    // Si le nombre de caractères présents dans la chaine est supérieur au nombre 
    // maximum, alors on découpe la chaine au nombre de caractères 
    if (strlen($str) > $nb) 
    {
        $str = substr($str, 0, $nb);
        $position_espace = strrpos($str, " "); //on récupère l'emplacement du dernier espace dans la chaine, pour ne pas découper un mot.
        $str = substr($str, 0, $position_espace);  //on redécoupe à la fin du dernier mot
        $str = $str."..."; //puis on rajoute des ...
    }
    return $str; //on retourne la variable modifiée
}

/* Fonctions pour login */
function verifUserBdd($login)
{
	// Vérifie l'identité d'un utilisateur 
	// dont les identifiants sont passes en paramètre
	// renvoie faux si user inconnu
	// renvoie l'id de l'utilisateur si succès

	$SQL="SELECT id FROM users WHERE email='$login'";

	return SQLGetChamp($SQL);
	// si on avait besoin de plus d'un champ
	// on aurait du utiliser SQLSelect
}

function verifMdp($login,$passe)
{
	$SQL="SELECT passe FROM users WHERE email='$login'";
	$passe_correct=SQLGetChamp($SQL);
	if ($passe == $passe_correct)
	{
		return true;
	}
	else return false;
}

function verifLogin($login)
{
	$login_valide = true;
	$i = 0;
	$users = listerUtilisateurs();
	$arrlength = count($users);
	while ($login_valide and $i < $arrlength)
	{
		$login_valide = ($login != $users[$i]['email']);
		$i++;
	}
	return $login_valide;
}

function ajouterUser($nom,$prenom,$login,$passe)
{
	$SQL="SET NAMES 'utf8'; INSERT INTO users (nom,prenom,email,passe) VALUES ('$nom','$prenom','$login','$passe')";
	SQLUpdate($SQL);
}

function connecteUser($login)
{
    $SQL="SET NAMES 'utf8'; UPDATE users SET connecte=1 WHERE email='$login'";
    SQLUpdate($SQL);
}


/* Fonctions pour users */
function listerUtilisateurs($classe = "both")
{
	// Lorsque la variable $classe vaut "both", elle renvoie tous les utilisateurs
	// Lorsqu'elle vaut "nv", elle ne renvoie que les utilisateurs non validés
	
	$SQL = "select * from users";
	if ($classe == "nv")
		$SQL .= " where valide=0";
	
	return parcoursRs(SQLSelect($SQL));
}

function validerUtilisateur($idUser)
{
	$SQL = "SET NAMES 'utf8'; UPDATE users SET valide=1 WHERE id='$idUser'";
	SQLUpdate($SQL);
}

function supprimerUtilisateur($idUser)
{
	$SQL = "DELETE FROM users WHERE id='$idUser'";
	SQLUpdate($SQL);
}


/* Fonctions pour footer */
function deconnecteUser($id)
{
    $SQL="SET NAMES 'utf8'; UPDATE users SET connecte=0 WHERE id='$id'";
    SQLUpdate($SQL);
}


/* Fonctions pour maLibSecurisation */
function isValide($id)
{
	$SQL="SELECT valide FROM users WHERE id='$id'";
	return SQLGetChamp($SQL)==1;
}

function isAdmin($id)
{
	$SQL="SELECT admin FROM users WHERE id='$id'";
	return SQLGetChamp($SQL)==1;
}


/* Fonctions pour compte */
function modifierCompte($nom,$prenom,$login,$passe)
{
	$id = $_SESSION["idUser"];
	$SQL="SET NAMES 'utf8'; UPDATE users SET nom='$nom', prenom='$prenom', email='$login',passe='$passe' WHERE id='$id'";
	SQLUpdate($SQL);
	$_SESSION["email"]=$login;
	$_SESSION["passe"]=$passe;
}


/* Fonctions pour projets */
function listerProjets($classe = "both")
{
	// Lorsque la variable $classe vaut "both", elle renvoie tous les projets
	// Lorsqu'elle vaut "m", elle ne renvoie que les projets modifiables
	// Lorsqu'elle vaut "c", elle ne renvoie que les projets consultables
	
	$idAuteur = $_SESSION["idUser"];
	$SQL = "select * from projet";
	if ($classe == "m")
		$SQL .= " where id_author='$idAuteur'";
	if ($classe == "c")
		$SQL .= " where id_author<>'$idAuteur'";
	
	return parcoursRs(SQLSelect($SQL));
}

function creerProjet($name,$date_delivery,$adress,$zip_code,$comment)
{
	$maj=date("Y-m-d H:i:s");
	$id_author=$_SESSION["idUser"];
	$SQL="SET NAMES 'utf8';
			INSERT INTO projet (name,date_delivery,date_modification,id_author,adress,zip_code,comment) VALUES ('$name','$date_delivery','$maj','$id_author','$adress','$zip_code','$comment')";
	SQLUpdate($SQL);
}

function modifierProjet($id_projet,$name,$date_delivery,$adress,$zip_code,$comment)
{
	$maj=date("Y-m-d H:i:s");
	$SQL="SET NAMES 'utf8';
			UPDATE optibuilding.projet SET name='$name', date_delivery='$date_delivery', date_modification='$maj', adress='$adress',
			zip_code='$zip_code', comment='$comment' WHERE id_project='$id_projet'";
	SQLUpdate($SQL);
}

function supprProjet($id_projet)
{
	global $BDD_host;
	global $BDD_user;
	global $BDD_password;
	
	$projet='projet_'.$id_projet;
	$SQL="DELETE FROM optibuilding.projet WHERE id_project='$id_projet'";
	SQLUpdate($SQL);
	try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
						   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
	catch (Exception $e)
	{die('Erreur : ' . $e->getMessage());}
	$req=$bdd->query('DROP DATABASE '.$projet.'');
}

function majProjet($id_projet)
{
	$maj=date("Y-m-d H:i:s");
	$SQL="SET NAMES 'utf8';
			UPDATE optibuilding.projet SET date_modification='$maj' WHERE id_project='$id_projet'";
	SQLUpdate($SQL);
}


/* Fonctions pour matériaux */
function listerMateriaux($classe = "both")
{
	// Lorsque la variable $classe vaut "both", elle renvoie tous les utilisateurs
	
	$SQL = "select * from materiaux";
	return parcoursRs(SQLSelect($SQL));
}

function ajouterMat($code_mat,$cupi_mat,$poste_mat,$type_mat,$libelle_mat,$fabricant_mat,$prix_unitaire_mat,$unite_mat,$duree_de_vie_mat,$taux_entretien_mat,$taux_remplacement_mat)
{
	$maj=date("Y-m-d");
	$SQL="SET NAMES 'utf8'; INSERT INTO materiaux (code_mat,MAJ_mat,CUPI_mat,poste_mat,type_mat,libelle_mat,fabricant_mat,prix_unitaire_mat,unite_mat,duree_de_vie_mat,taux_entretien_mat,taux_remplacement_mat)
			VALUES ('$code_mat','$maj','$cupi_mat','$poste_mat','$type_mat','$libelle_mat','$fabricant_mat','$prix_unitaire_mat','$unite_mat','$duree_de_vie_mat','$taux_entretien_mat','$taux_remplacement_mat')";
	SQLUpdate($SQL);
}

function supprimerMat($idMat)
{
	$SQL = "DELETE FROM materiaux WHERE id_mat='$idMat'";
	SQLUpdate($SQL);
}

function modifierMat($code_mat,$cupi_mat,$poste_mat,$type_mat,$libelle_mat,$fabricant_mat,$prix_unitaire_mat,$unite_mat,$duree_de_vie_mat,$taux_entretien_mat,$taux_remplacement_mat)
{
	$maj=date("Y-m-d");
	$id=$_SESSION['idMat'];
	$SQL="SET NAMES 'utf8';
			UPDATE materiaux SET code_mat='$code_mat', MAJ_mat='$maj', CUPI_mat='$cupi_mat', poste_mat='$poste_mat', type_mat='$type_mat', libelle_mat='$libelle_mat', fabricant_mat='$fabricant_mat',
			prix_unitaire_mat='$prix_unitaire_mat', unite_mat='$unite_mat', duree_de_vie_mat='$duree_de_vie_mat', taux_entretien_mat='$taux_entretien_mat', taux_remplacement_mat='$taux_remplacement_mat'
			WHERE id_mat='$id'";
	SQLUpdate($SQL);
}

function importerMat($bdd,$id_mat,$quantite)
{
	$id_sce=$_SESSION['scenario'];
	$id_piece=$_SESSION['piece'];
	$SQL="SET NAMES 'utf8';
			INSERT INTO $bdd.articles (code_article,MAJ_article,CUPI_article,poste,type_materiau,libelle,fabricant,unite,prix_unitaire,duree_de_vie,taux_entretien,taux_remplacement)
			SELECT code_mat,MAJ_mat,CUPI_mat,poste_mat,type_mat,libelle_mat,fabricant_mat,unite_mat,prix_unitaire_mat,duree_de_vie_mat,taux_entretien_mat,taux_remplacement_mat
			FROM optibuilding.materiaux WHERE id_mat='$id_mat'";
	SQLUpdate($SQL);
	
	$SQL="SELECT MAX(id_article) FROM $bdd.articles";
	$id_art=SQLGetChamp($SQL);
	$SQL="SET NAMES 'utf8'; UPDATE $bdd.articles SET id_scenario='$id_sce', id_piece='$id_piece', surface='$quantite' WHERE id_article='$id_art'";
	SQLUpdate($SQL);
}


/* Fonctions pour pièces */
function creerPiece($bdd,$nom_piece,$surface,$comment)
{
	$SQL="SET NAMES 'utf8'; INSERT INTO $bdd.pieces (nom_piece,surface,comment_piece) VALUES ('$nom_piece','$surface','$comment')";
	SQLUpdate($SQL);
	
	$maj=date("Y-m-d H:i:s");
	$id_project=$_SESSION['projet'];
	$SQL="SET NAMES 'utf8'; UPDATE optibuilding.projet SET date_modification='$maj' WHERE id_project='$id_project'";
	SQLUpdate($SQL);
}

function modifierPiece($bdd,$id_piece,$nom_piece,$surface,$comment)
{
	$SQL="SET NAMES 'utf8';
			UPDATE $bdd.pieces SET nom_piece='$nom_piece', surface='$surface', comment_piece='$comment' WHERE id_piece='$id_piece'";
	SQLUpdate($SQL);
}

function supprPiece($bdd,$id_piece)
{
	$SQL="DELETE FROM $bdd.pieces WHERE id_piece='$id_piece'";
	SQLUpdate($SQL);
}


/* Fonctions pour scénarios */
function creerScenario($bdd,$id_piece,$nom_scenario,$comment_scenario)
{
	$SQL="SET NAMES 'utf8'; INSERT INTO $bdd.scenarios (id_piece,nom_scenario,comment_scenario) VALUES ('$id_piece','$nom_scenario','$comment_scenario')";
	SQLUpdate($SQL);
	
	$maj=date("Y-m-d H:i:s");
	$id_project=$_SESSION['projet'];
	$SQL="SET NAMES 'utf8'; UPDATE optibuilding.projet SET date_modification='$maj' WHERE id_project='$id_project'";
	SQLUpdate($SQL);
	
	//A la création d'un scénario, on insère une ligne dans la table 'resultats', cette ligne sera mise à jour ensuite.
	$SQL="SELECT MAX(id_scenario) FROM $bdd.scenarios";
	$id_sce=SQLGetChamp($SQL);
	
	$SQL="SET NAMES 'utf8'; INSERT INTO $bdd.resultats (id_scenario,id_piece) VALUES ('$id_sce','$id_piece')";
	SQLUpdate($SQL);
}

function copierScenario($projet,$id_scenario,$id_piece,$nom_scenario,$comment_scenario)
{
	global $BDD_host;
	global $BDD_user;
	global $BDD_password;
	
	//On ajoute la copie dans la table scenarios.
	$nom=$nom_scenario.'_Copie';
	$SQL="SET NAMES 'utf8'; INSERT INTO $projet.scenarios (id_piece,nom_scenario,comment_scenario) VALUES ('$id_piece','$nom','$comment_scenario')";
	SQLUpdate($SQL);
	
	$SQL="SELECT MAX(id_scenario) FROM $projet.scenarios";
	$id_sce=SQLGetChamp($SQL);
	
	//On sélectionne et on insère tous les matériaux qui nous intéressent.
	try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                                       array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
		
	$req=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'"');
 
    while($donnees=$req->fetch())
    {
        $request=$bdd->prepare('INSERT INTO articles(id_scenario, id_piece, code_article, poste, CUPI_article, MAJ_article, surface,
                               type_materiau, libelle, fabricant, unite, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement)
                                VALUES(:id_scenario, :id_piece, :code_article, :poste, :CUPI_article, :MAJ_article, :surface,
                                :type_materiau, :libelle, :fabricant, :unite, :prix_unitaire, :duree_de_vie, :taux_entretien, :taux_remplacement)');

        $request->execute(array('id_scenario'=>$id_sce, 'id_piece'=>$id_piece, 'code_article'=>$donnees['code_article'],
								'poste'=>$donnees['poste'], 'CUPI_article'=>$donnees['CUPI_article'],
								'MAJ_article'=>$donnees['MAJ_article'], 'surface'=>$donnees['surface'],
								'type_materiau'=>$donnees['type_materiau'],
                            	'libelle'=>$donnees['libelle'], 'fabricant'=>$donnees['fabricant'],
								'unite'=>$donnees['unite'], 'prix_unitaire'=>$donnees['prix_unitaire'],
								'duree_de_vie'=>$donnees['duree_de_vie'],
								'taux_entretien'=>$donnees['taux_entretien'],
								'taux_remplacement'=>$donnees['taux_remplacement']
                            ));
    }
	
	//A la création d'un scénario, on insère une ligne dans la table 'resultats', cette ligne sera mise à jour ensuite.
	$SQL="SET NAMES 'utf8'; INSERT INTO $projet.resultats (id_scenario,id_piece) VALUES ('$id_sce','$id_piece')";
	SQLUpdate($SQL);
}

function copierSceMaj($projet,$id_scenario,$id_piece,$nom_scenario,$comment_scenario)
{
	global $BDD_host;
	global $BDD_user;
	global $BDD_password;
	
	//On ajoute la copie dans la table scenarios.
	$nom=$nom_scenario.'_Copie_MAJ';
	$SQL="SET NAMES 'utf8'; INSERT INTO $projet.scenarios (id_piece,nom_scenario,comment_scenario) VALUES ('$id_piece','$nom','$comment_scenario')";
	SQLUpdate($SQL);
	
	$SQL="SELECT MAX(id_scenario) FROM $projet.scenarios";
	$id_sce=SQLGetChamp($SQL);
	
	//On se connecte à la BDD du projet en cours
    try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
		
	// On se connecte à la bdd optibuilding car nous voulons accèder à  la table 'materiaux'
    try {$optibuilding= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8', $BDD_user, $BDD_password,
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
		
	//On recherche les articles utilisés par le code_mat et on insère les nouveaux articles (avec les mises à jour éventuelles)
    $req=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'"');
    
    while($donnees=$req->fetch())
    {
		$code_art=$donnees['code_article'];
        $req_mat=$optibuilding->query('SELECT * FROM materiaux WHERE code_mat="'.$code_art.'"');
        
        $donnees_mat=$req_mat->fetch();
        
        $request=$bdd->prepare('INSERT INTO articles(id_scenario, id_piece, code_article, poste, CUPI_article, MAJ_article, surface,
                                type_materiau, libelle, fabricant, unite, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement)
                                
                                VALUES(:id_scenario, :id_piece, :code_article, :poste, :CUPI_article, :MAJ_article, :surface,
                                :type_materiau, :libelle, :fabricant, :unite, :prix_unitaire, :duree_de_vie, :taux_entretien,
                                :taux_remplacement)');
       
        $request->execute(array('id_scenario'=>$id_sce, 'id_piece'=>$id_piece, 'code_article'=>$donnees['code_article'],
                            'CUPI_article'=>$donnees_mat['CUPI_mat'], 'MAJ_article'=>$donnees_mat['MAJ_mat'],
                            'surface'=>$donnees['surface'],
                            'poste'=>$donnees_mat['poste_mat'], 'type_materiau'=>$donnees_mat['type_mat'],
                            'libelle'=>$donnees_mat['libelle_mat'], 'fabricant'=>$donnees_mat['fabricant_mat'],
                            'unite'=>$donnees_mat['unite_mat'], 'prix_unitaire'=>$donnees_mat['prix_unitaire_mat'],
                            'duree_de_vie'=>$donnees_mat['duree_de_vie_mat'],
                            'taux_entretien'=>$donnees_mat['taux_entretien_mat'],
                            'taux_remplacement'=>$donnees_mat['taux_remplacement_mat']
                            ));
    }
	
	//A la création d'un scénario, on insère une ligne dans la table 'resultats', cette ligne sera mise à jour ensuite.
	$SQL="SET NAMES 'utf8'; INSERT INTO $projet.resultats (id_scenario,id_piece) VALUES ('$id_sce','$id_piece')";
	SQLUpdate($SQL);
}

function modifierSce($bdd,$id_scenario,$name,$comment)
{
	$SQL="SET NAMES 'utf8';
			UPDATE $bdd.scenarios SET nom_scenario='$name', comment_scenario='$comment' WHERE id_scenario='$id_scenario'";
	SQLUpdate($SQL);
}

function supprSce($bdd,$id_scenario)
{
	$SQL="DELETE FROM $bdd.scenarios WHERE id_scenario='$id_scenario'";
	SQLUpdate($SQL);
}

/* Fonctions pour articles */
function ajouterArt($bdd,$code_mat,$cupi,$poste,$type_materiau,$libelle,$fabricant,$surface,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement)
{
	$id_sce=$_SESSION['scenario'];
	$id_piece=$_SESSION['piece'];
	$maj=date("Y-m-d");
	$SQL="SET NAMES 'utf8'; INSERT INTO $bdd.articles (id_scenario,id_piece,code_article,MAJ_article,CUPI_article,surface,poste,type_materiau,libelle,fabricant,prix_unitaire,unite,duree_de_vie,taux_entretien,taux_remplacement)
			VALUES ('$id_sce','$id_piece','$code_mat','$maj','$cupi','$surface','$poste','$type_materiau','$libelle','$fabricant','$prix_unitaire','$unite','$duree_de_vie','$taux_entretien','$taux_remplacement')";
	SQLUpdate($SQL);
}

function modifierArt($bdd,$id_article,$code_article,$cupi,$poste,$type_materiau,$libelle,$fabricant,$surface,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement)
{
	$id_sce=$_SESSION['scenario'];
	$id_piece=$_SESSION['piece'];
	$SQL="SET NAMES 'utf8';
			UPDATE $bdd.articles SET id_scenario='$id_sce', id_piece='$id_piece', code_article='$code_article',
			CUPI_article='$cupi', surface='$surface', poste='$poste', type_materiau='$type_materiau', libelle='$libelle',
			fabricant='$fabricant', prix_unitaire='$prix_unitaire', unite='$unite', duree_de_vie='$duree_de_vie', taux_entretien='$taux_entretien', taux_remplacement='$taux_remplacement'
			WHERE id_article='$id_article'";
	SQLUpdate($SQL);
}

function supprimerArt($bdd,$id_article)
{
	$SQL = "DELETE FROM $bdd.articles WHERE id_article='$id_article'";
	SQLUpdate($SQL);
}

function majArt($bdd,$id_article,$MAJ_article,$CUPI_article,$poste,$type_materiau,$libelle,$fabricant,$prix_unitaire,$unite,$duree_de_vie,$taux_entretien,$taux_remplacement)
{
	$SQL="SET NAMES 'utf8';
			UPDATE $bdd.articles SET MAJ_article='$MAJ_article', CUPI_article='$CUPI_article', poste='$poste', type_materiau='$type_materiau', libelle='$libelle',
			fabricant='$fabricant', prix_unitaire='$prix_unitaire', unite='$unite', duree_de_vie='$duree_de_vie', taux_entretien='$taux_entretien', taux_remplacement='$taux_remplacement'
			WHERE id_article='$id_article'";
	SQLUpdate($SQL);
}

/* Fonctions pour infos_projet */
function saveInfos($bdd,$taux_inflation,$duree_exploitation,$cout_ext_fixe,$cout_ext_annuel,$comment,$cout_externalite,$cout_externalite_courant)
{
	$SQL="SET NAMES 'utf8';
			UPDATE $bdd.informations SET taux_inflation='$taux_inflation', duree_exploitation='$duree_exploitation', cout_ext_fixe='$cout_ext_fixe',
			cout_ext_annuel='$cout_ext_annuel', comment='$comment', cout_externalite='$cout_externalite', cout_externalite_courant='$cout_externalite_courant'";
	SQLUpdate($SQL);
}

/* Fonctions pour résultats */
function saveResultats($projet,$piece,$scenario,$cout_construction,$cout_remplacement,$cout_maintenance,$cout_global,$cout_remplacement_courant,$cout_maintenance_courant,$cout_global_courant)
{
	$bdd='projet_'.$projet;
	$SQL="SET NAMES 'utf8';
			UPDATE $bdd.resultats SET cout_construction='$cout_construction', cout_remplacement='$cout_remplacement',
			cout_maintenance='$cout_maintenance', cout_global='$cout_global', cout_remplacement_courant='$cout_remplacement_courant',
			cout_maintenance_courant='$cout_maintenance_courant', cout_global_courant='$cout_global_courant'
			WHERE id_scenario='$scenario' AND id_piece='$piece'";
	SQLUpdate($SQL);
}
?>