<!-- Cette page affiche un formulaire de modification d'un matériau de la base de données.
On récupère l'id du matériau via l'URL puis on affiche un formulaire prérempli avec les infos sur le matériau-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "modification_mat.php")
{
	header("Location:../index.php?view=modification_mat");
	die("");
}
include_once "libs/config.php";
?>

<p>Cette page vous permet de mofifier un matériau dans la base de données.</p>
<form action='controleur.php'>
<button type="submit" name="action" value="retour_table" class="icones left_arrow"> Retour à la table</button>
</form>
<form action='controleur.php' class='formulaire'>
    <p>
        <fieldset>
            <?php  try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8', $BDD_user, $BDD_password,
                                        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
                    catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
        
            $id_mat=htmlentities($_GET['mat']);
            $_SESSION['idMat']=$id_mat;
        
            $affiche=$bdd->query('SELECT* FROM materiaux WHERE id_mat='.$id_mat.'');
            $mat=$affiche->fetch()
            ?>                    
            <legend>Pour modifier un matériau</legend>
			
			<p class='red'>* Champs obligatoires.</p></br>
            <ol>
			<li>
            <label class='label3' for='code_mat'>Code du matériau <span class='red'>*</span></label>
            <input type='text' id='code_mat' name='code_mat' value="<?php echo $mat['code_mat']; ?>"/></br></br>
			</li>
			<li>
			<label class='label3' for='cupi'>Le matériau est-il répertorié au CUPI ?</label>
            <input type='radio' name='cupi' value='oui' <?php if ($mat['CUPI_mat'] == "oui") { ?> checked <?php }; ?>> Oui
			<input type='radio' name='cupi' value=""> Non</br></br>
            </li>
			<li>    
            <label class='label3' for='poste'>Poste <span class='red'>*</span> <em>(Sol, Toiture, Vêture...)</em></label>
            <input type='text' id='poste' name='poste' value="<?php echo $mat['poste_mat']; ?>"/></br></br>
            </li>
			<li>    
            <label class='label3' for='type_materiau'>Type du matériau <span class='red'>*</span></label>
            <input type='text' id='type_materiau' name='type_materiau' value='<?php echo $mat['type_mat']; ?>'/></br></br>
            </li>
			<li>    
            <label class='label3' for='libelle'>Libellé <span class='red'>*</span></label>
            <textarea name='libelle' rows="4" cols="30"><?php echo $mat['libelle_mat']; ?></textarea></br></br>
            </li>
			<li>    
            <label class='label3' for='fabricant'>Fabricant</label>
            <input type='text' id='fabricant' name='fabricant' value='<?php echo $mat['fabricant_mat']; ?>'/></br></br>
            </li>
			<li>    
            <label class='label3' for='prix_unitaire'>Prix unitaire <span class='red'>*</span> </em></label>
            <input type='numbre' id='prix_unitaire' name='prix_unitaire' value='<?php echo $mat['prix_unitaire_mat']; ?>' style="width:100px;"/>
                
            <select name="unite" id="unite">
                <option value="m" <?php if ($mat['unite_mat'] == "m") { ?> selected="selected" <?php }; ?>>m</option>
                <option value="m2" <?php if ($mat['unite_mat'] == "m2") { ?> selected="selected" <?php }; ?>>m2</option>
                <option value="unitaire" <?php if ($mat['unite_mat'] == "unitaire") { ?> selected="selected" <?php }; ?>>unitaire</option>
            </select></br></br>
            </li>
			<li>    
            <label class='label3' for='duree_de_vie'>Durée de vie <span class='red'>*</span></label>
            <input type='text' id='duree_de_vie' name='duree_de_vie' value='<?php echo $mat['duree_de_vie_mat']; ?>' style="width:100px;"/> années</br></br>
            </li>
			<li>    
            <label class='label3' for='taux_entretien'>Taux d'entretien annuel</label>
            <input type='number' id='taux_entretien' name='taux_entretien' value="<?php echo $mat['taux_entretien_mat']; ?>" min="0" step="0.0001" style="width:100px;"/></br></br>
            </li>
			<li>    
            <label class='label3' for='taux_remplacement'>Taux de remplacement</label>
            <input type='number' id='taux_remplacement' name='taux_remplacement' value="<?php echo $mat['taux_remplacement_mat']; ?>" min="0" step="0.0001" style="width:100px;"/></br></br>
            </li>
			  
            <center><button type="submit" name="action" value="modifier_mat" class='icones edit formule'> Modifier</button></center>
            </br></br>
            </ol>            
        </fieldset>
    </p>
  </form>