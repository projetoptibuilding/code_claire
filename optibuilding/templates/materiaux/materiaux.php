<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "materiaux.php")
{
	header("Location:../index.php?view=materiaux");
	die("");
}

include_once("libs/maLibUtils.php");
include_once "libs/config.php";
?>


<h1><span>La base de données des matériaux</span></h1>

<?php
    try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
?>

<p>
	<table>
		
            <thead>
                <tr>
                    <th class="codemat">Code Matériau</th>
					<th class="maj">MAJ</th>
					<th class="cupi">CUPI</th>
                    <th class="poste">Poste</th>
                    <th class="typ">Type</th>
                    <th class="libel">Libellé</th>
                    <th class="fab">Fabricant</th>
                    <th class="price">Prix unitaire</th>
                    <th class="unit">Unité</th>
                    <th class="duree">Durée de vie</th>
                    <th class="taux">Taux entretien</th>
                    <th class="taux">Taux remplacement</th>
                </tr>
            </thead>
                     
            <tbody>
                                                       
			<?php
				$messagesParPage=20; //Nous allons afficher 20 entrées par page.

				$retour_total=$bdd->query('SELECT COUNT(*) AS total FROM materiaux');
				$donnees_total=$retour_total->fetch(PDO::FETCH_ASSOC);
				$total=$donnees_total['total']; //On récupère le total pour le placer dans la variable $total.
            
				//Nous allons maintenant compter le nombre de pages.
				$nombreDePages=ceil($total/$messagesParPage);
            
				if(isset($_GET['page'])) // Si la variable $_GET['page'] existe... Afficher une erreur si jamais on entre une mauvaise URL
				{
					$pageActuelle=intval($_GET['page']);
            
					if($pageActuelle>$nombreDePages) // Si la valeur de $pageActuelle (le numéro de la page) est plus grande que $nombreDePages...
					{
						$pageActuelle=$nombreDePages;
					}
				}
				else
				{
					$pageActuelle=1; // La page actuelle est la n°1    
				}
            
				$premiereEntree=($pageActuelle-1)*$messagesParPage; // On calcule la première entrée à lire

				$affiche=$bdd->query('SELECT* FROM materiaux ORDER BY code_mat LIMIT '.$premiereEntree.', '.$messagesParPage.'');
    
				while($donnees=$affiche->fetch())
				{?>
					<tr>
						<td><?php echo $donnees['code_mat']; ?></td>
						<td><?php echo $donnees['MAJ_mat']; ?></td>
						<td><?php echo $donnees['CUPI_mat']; ?></td>
						<td><?php echo $donnees['poste_mat']; ?></td>
						<td><?php echo $donnees['type_mat']; ?></td>
						<td><?php echo $donnees['libelle_mat']; ?></td>
						<td><?php echo $donnees['fabricant_mat']; ?></td>
						<td><?php echo $donnees['prix_unitaire_mat']; ?></td>
						<td><?php echo $donnees['unite_mat']; ?></td>
						<td><?php echo $donnees['duree_de_vie_mat']; ?></td>
						<td><?php echo $donnees['taux_entretien_mat']; ?></td>
						<td><?php echo $donnees['taux_remplacement_mat']; ?></td>
					</tr>
               
				<?php }
				echo '<p class="centrer">Page : '; //Pour l'affichage, on centre la liste des pages
				for($i=1; $i<=$nombreDePages; $i++) //On fait notre boucle
				{
					//On va faire notre condition
					if($i==$pageActuelle) //Si il s'agit de la page actuelle...
					{echo ' [ '.$i.' ] ';}	
					else //Sinon...
					{echo ' <a class="liennoir" href="index.php?view=materiaux&page='.$i.'">'.$i.'</a> ';}
				}
				echo '</p>';
				?>
				
            </tbody> 
    </table>
</p>

<br/>
<br/>

<?php if (valider("admin","SESSION"))
{?>
	<form action='controleur.php'>
		
		<center><button type="submit" name="action" value="ajouter_mat" class="icones add tall1">Ajouter un matériau</button>
		<button type="submit" name="action" value="modif_ret_mat" class="icones delete tall1">Modifier ou Retirer un matériau</button></center>

	</form>
<?php } ?>
