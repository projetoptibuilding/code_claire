<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "insertion_mat.php")
{
	header("Location:../index.php?view=insertion_mat");
	die("");
}

?>


<h1><span>Créer un nouveau matériau dans la base de données<span></h1>

<!-- Cette page comporte un formulaire qui permet d'insérer un nouveau matériau dans la base de données-->
            
<form action='controleur.php' class="formulaire2">
    <p>
        <fieldset>
            
            
			<p class='red'>* Champs obligatoires.</p></br>
			
			<ol>
				<li>
				<label class='label3' for='code_mat'>Code du matériau <span class='red'>*</span></label>
				<input type='text' id='code_mat' name='code_mat'/></br></br>
				</li>
				
				<li>
				<label  class='label3' for='cupi'>Le matériau est-il répertorié au CUPI ?</label>
				<input type='radio' name='cupi' value='oui'> Oui
				<input type='radio' name='cupi' value=""> Non</br></br>
				</li>
				
				<li>
				<label class='label3' for='poste'>Poste <span class='red'>*</span> <em>(Sol, Toiture, Vêture...)</em></label>
				<input type='text' id='poste' name='poste'/></br></br>
				</li>
            
				<li>
				<label class='label3' for='type_materiau'>Type du matériau <span class='red'>*</span></label>
				<input type='text' id='type_materiau' name='type_materiau'/></br></br>
				</li>
				
				<li>
				<label class='label3' for='libelle'>Libellé <span class='red'>*</span></label>
				<textarea name='libelle' rows="4" cols="45" placeholder='Libellé'></textarea></br></br>
				</li>
				
				<li>
				<label class='label3' for='fabricant'>Fabricant</label>
				<input type='text' id='fabricant' name='fabricant'/></br></br>
				</li>
				
				<li>
				<label class='label3' for='prix_unitaire'>Prix unitaire <span class='red'>*</span> </label>
				<input type='numbre' id='prix_unitaire' name='prix_unitaire' style="width:100px;"/>

				<select name="unite" id="unite">
				    <option value="m">m</option>
				    <option value="m2" selected="selected">m2</option>
				    <option value="unitaire">unitaire</option>
				</select></br></br>
				</li>
				
				<li>
				<label class='label3' for='duree_de_vie'>Durée de vie <span class='red'>*</span></label>
				<input type='text' id='duree_de_vie' name='duree_de_vie' style="width:100px;"/> années</br></br>
				</li>
            
				<li>
				<label class='label3' for='taux_entretien'>Taux d'entretien annuel</label>
				<input type='number' id='taux_entretien' name='taux_entretien' value="0.03" min="0" step="0.0001" style="width:100px;"/></br></br>
				</li>
            
				<li>
				<label class='label3' for='taux_remplacement'>Taux de remplacement</label>
				<input type='number' id='taux_remplacement' name='taux_remplacement' value="1" min="0" step="0.0001" style="width:100px;"/></br></br>
				</li>
				
				
				<center><button type="submit" name="action" value="creer_mat" class="icones add formule1 notPrint"> Créer le matériau</button>
				<button type="reset" class="icones formule1 notPrint"> Remettre le formulaire à zéro</button><center>
				</br></br>
				
                
			</ol>	
        </fieldset>
    </p>
    
</form>

<button class="icones imprimer notPrint" onclick="window.print();"> Imprimer</button></br>

<form action='controleur.php' >
  <button type="submit" name="action" value="retour_table" class="icones left_arrow notPrint">  Retour à la table</button>
 </form>