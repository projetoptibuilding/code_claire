<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "fiche_article.php")
{
	header("Location:../index.php?view=fiche_article");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php
        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['sce']);
        $mat=htmlentities($_GET['mat']);
		
		$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
		$nom_piece=selectChamp($projet.'.pieces',"nom_piece","id_piece",$id_piece);
		$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);

        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
        
        
        $affiche=$bdd->query('SELECT* FROM articles WHERE id_article="'.$mat.'" AND id_piece='.$id_piece.' AND id_scenario='.$id_scenario.'');
        $article=$affiche->fetch()
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
	<a class='liennoir' href='index.php?view=analyse&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Analyse des coûts</a> >
	<a class='liennoir' href='index.php?view=fiche_article&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>&mat=<?php echo($mat); ?>'>Fiche de l'article <?php echo($article['code_article']); ?></a>
</p>
 
    <body>
        <section>
        <p class='ficheart'>Fiche matériau <?php echo $article['code_article'];?></p>

                <label class='label5'>CUPI :</label> <?php echo $article['CUPI_article'];?></br></br>
                
                <label class='label5'>Type du matériau : </label><?php echo $article['type_materiau']; ?></br></br>
                
                <label class='label5'> Libellé :</label> <?php echo $article['libelle']; ?></br></br>
                
                <label class='label5'> Fabricant :</label><?php echo $article['fabricant']; ?></br></br>
                
                <label class='label5'> Quantité :</label><?php echo $article['surface'].' '.$article['unite'];?></br></br>
                           
                <label class='label5'> Prix unitaire :</label> <?php echo $article['prix_unitaire'];?> €</br></br>
                
               <label class='label5'>  Durée de vie : </label><?php echo $article['duree_de_vie']; ?> années</br></br>
                
                <label class='label5'> Taux d'entretien annuel : </label><?php echo $article['taux_entretien']; ?></br></br>
                
                <label class='label5'> Taux de remplacement : </label><?php echo $article['taux_remplacement']; ?></br></br>
        </p>
        
        <p><form action='controleur.php'>
                <input type='hidden' name='id_article' value='<?php echo $article['id_article'];?>'/>
				<?php
                $idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_projet);
                if ($idAuteur == $_SESSION['idUser'])
                {
                ?>
                <center><button type="submit" name="action" value="modif_art" onclick="return confirm('Attention vous allez quitter votre analyse du calcul.\nOK pour continuer\nAnnuler pour rester sur la page');" class='icones edit formule'> Modifier</button></center></br>
                <?php }; ?>
				<button type="submit" name="action" value="analyse" class="icones left_arrow"> Retour</button></br>
        </form></p>
        
        </section>

    </body>
