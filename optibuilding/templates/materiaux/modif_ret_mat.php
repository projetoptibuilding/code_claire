<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "modif_ret_mat.php")
{
	header("Location:../index.php?view=modif_ret_mat");
	die("");
}
include_once "libs/config.php";
?>

<h1><span>La base de données des matériaux</span></h1>

<!-- Cette page offre la possibilité de choisir un matériau que l'on veut modifier ou retirer de notre base de données
On se connecte à la BDD qui contient tous les matériaux et on les affiche dans un tableau, chaque ligne contient deux boutons : un pour modifier les propriétés,
l'autre pour retirer le matériau de la liste-->
                    
<?php   try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
        
        $affiche=$bdd->query('SELECT* FROM materiaux ORDER BY code_mat');
?>

<form action='controleur.php'>               
  <button type="submit" name="action" value="retour_table" class="icones left_arrow"> Retour à la table</button>   
</form>

<p>
    <table>
        <thead>
            <tr>
                <th></th>
                <th>Code Matériau</th>
				<th>MAJ</th>
				<th>CUPI</th>
                <th>Poste</th>
                <th>Type</th>
                <th>Libellé</th>
                <th>Fabricant</th>
                <th>Prix unitaire</th>
                <th>Unité</th>
                <th>Durée de vie</th>
                <th>Taux entretien</th>
                <th>Taux remplacement</th>
            </tr>
        </thead>
                     
        <tbody>                                        
            <?php   while($donnees=$affiche->fetch())
            {?>
                <tr>
                    <td class='bouton'>
                        <a href="index.php?view=modification_mat&mat=<?php echo $donnees['id_mat']; ?>" > <button type="submit" name="action" value="ajouter_mat" class="icones edit small">Modifier</button></a>
                    <!-- On atteint la page de mofification de matériau en faisant passer l'id du matériau par l'URL -->                   
                        <a href="index.php?view=retirer_mat&mat=<?php echo $donnees['id_mat']; ?>" onclick="return confirm('Voulez-vous vraiment supprimer ce matériau ? Cette action est irréversible.');" >  <button type="submit" name="action" value="ajouter_mat" class="icones delete small">Retirer</button></a>
                    <!-- On atteint le code de suppression de l'entrée choisie, au moment du clic, une page d'avertissement s'affiche et demande de confirmer l'action -->                                
                    </td>
                    <td><?php echo $donnees['code_mat']; ?></td>
					<td><?php echo $donnees['MAJ_mat']; ?></td>
					<td><?php echo $donnees['CUPI_mat']; ?></td>
					<td><?php echo $donnees['poste_mat']; ?></td>
					<td><?php echo $donnees['type_mat']; ?></td>
					<td><?php echo $donnees['libelle_mat']; ?></td>
					<td><?php echo $donnees['fabricant_mat']; ?></td>
					<td><?php echo $donnees['prix_unitaire_mat']; ?></td>
					<td><?php echo $donnees['unite_mat']; ?></td>
					<td><?php echo $donnees['duree_de_vie_mat']; ?></td>
					<td><?php echo $donnees['taux_entretien_mat']; ?></td>
					<td><?php echo $donnees['taux_remplacement_mat']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</p>
</br></br>