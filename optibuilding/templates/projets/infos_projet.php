<!--
Cette page présente un formulaire pour rensigner des informations relatives au projet en cours : la durée d'exploitaiton, le taux d'inflation
et les externalités.
-->

<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "infos_projet.php")
{
	header("Location:../index.php?view=infos_projet");
	die("");
}

include_once "libs/maLibUtils.php";
include_once "libs/modele.php";
include_once "templates/calcul/fonctions_cout_global.php";
include_once "libs/config.php";
?>


        
<body>
    <?php
        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
		$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
		
		if(isset($_POST['taux_inflation']) && isset($_POST['duree_exploitation'])
		   && isset($_POST['autre_annuel']) && isset($_POST['autre']) && isset($_POST['comment']))
        {
            $duree_exp=htmlentities($_POST['duree_exploitation']);
            $autre=htmlentities($_POST['autre']);
            $autre_annuel=htmlentities($_POST['autre_annuel']);
            $comment=htmlentities($_POST['comment']);
            $taux_inflation=htmlentities($_POST['taux_inflation']);
            $coeff_inflation=1+$taux_inflation/100;
            $coeff_nul=1;    
            
            $externalities=sprintf('%.2f', (externality_cost($duree_exp,$coeff_nul,$autre_annuel)+$autre));
            $externalities_courant=sprintf('%.2f', (externality_cost($duree_exp,$coeff_inflation,$autre_annuel)+$autre));
        }
		else
		{
            try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                                   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
            catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
            $req=$bdd->query('SELECT * FROM informations');
            $donnees=$req->fetch();
            
            $taux_inflation=$donnees['taux_inflation'];
            $duree_exp=$donnees['duree_exploitation'];
            $autre=$donnees['cout_ext_fixe'];
            $autre_annuel=$donnees['cout_ext_annuel'];
            $comment=$donnees['comment'];
            $externalities=$donnees['cout_externalite'];
            $externalities_courant=$donnees['cout_externalite_courant'];
		}   
    ?>
	
	<p>
	<a class="liennoir" href='index.php?view=projets'>Projets</a> >
	<a class="liennoir" href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<?php if(isset($_GET['sce']))
	{
		$id_piece=htmlentities($_GET['piece']);
		$id_sce=htmlentities($_GET['sce']);
		$nom_piece=selectChamp($projet.'.pieces',"nom_piece","id_piece",$id_piece);
		$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_sce);
		?>
		<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
		<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_sce); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
		<a class='liennoir' href='index.php?view=infos_projet&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_sce); ?>'>Informations sur le projet</a>
	<?php }
	else { ?>
		<a class="liennoir" href='index.php?view=infos_projet&projet=<?php echo($id_projet); ?>'>Informations sur le projet</a>
	<?php }; ?>
	</p>
	
    <form action="controleur.php">
		<?php if(isset($_GET['sce']))
		{ ?>
			<button type="submit" name="action" value="retour_sce_id" class="icones left_arrow"> Retour au scénario</button></br></br>
		<?php }
		else { ?>
			<button type="submit" name="action" value="retour_pieces" class="icones left_arrow ">  Retour aux pièces</button></br></br>
		<?php }; ?>
	</form>
    
	<p class='red'>* Champs obligatoires</p>
	
    <p>
    <form method="post" class='formulaire' action="">
        <ol>
        <fieldset>
		
            <legend>Taux</legend>
			<li>
            <label class='label4 for='taux_inflation'>Taux d'inflation </label>
            <input type='number' id='taux_inflation' name='taux_inflation' value='<?php if(isset($taux_inflation) and $taux_inflation != 0){echo $taux_inflation;}else{echo 2.0;};?>' step="0.01" style="width:50px;"/> %</br></br>

            </li>
			<li>
            <label class='label4' for='duree_exploitation'>Durée d'exploitation <span class='red'>*</span></label>
            <input type='number' id='duree_exploitation' name='duree_exploitation' value='<?php echo $duree_exp;?>' min="0" step="0.1" style="width:50px;"/> ans
			</li>
		
        </fieldset>
        </ol>
		<ol>
        <fieldset>
		
            <legend>Externalités</legend>
            <li>
            <label class='label4' for='autre'>Coûts fixes </label>
            <input type='number' id='autre' name='autre' value='<?php echo $autre;?>' step='0.01' style="width:80px;"/> €</br></br>
           </li>
			<li>
            <label class='label4'for='autre_annuel'>Coûts annuels </label>
            <input type='number' id='autre_annuel' name='autre_annuel' value='<?php echo $autre_annuel;?>' step='0.01' style="width:80px;"/> €</br></br>
			</li>
            
			<li>
			<label class='label4' for='comment'>Commentaires </label>
            <textarea name='comment' rows='3' cols='50'/><?php echo $comment; ?></textarea></br></br>
			</li>
            
       
		
        </fieldset>
        </ol>
        <p><center><button type='submit' class='icones calculer formule'> Calculer </button></center></p>
        
	</form>	
		
	Coût des externalités : <?php echo number_format($externalities,2,"."," ");?> €</br>
    Coût courant des externalités : <?php echo number_format($externalities_courant,2,"."," ");?> €</br>  
	</p>
	

<?php
$idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_projet);
if ($idAuteur == $_SESSION['idUser'])
{
?>   
    <p>
		<p class='red'>ATTENTION ! Vous devez avoir cliqué sur le bouton "Calculer" avant d'enregistrer.</p></br>
		<form action="controleur.php">
			<?php if(isset($_GET['sce']))
			{ ?>
				<input type='hidden' name='id_scenario' value='<?php echo (htmlentities($_GET['sce']));?>' />
			<?php }; ?>
            <input type='hidden' name='taux_inflation' value='<?php echo $taux_inflation;?>' />
            <input type='hidden' name='duree_exploitation' value='<?php echo $duree_exp;?>'/>
            <input type='hidden' name='cout_ext_fixe' value='<?php echo $autre;?>'/>
            <input type='hidden' name='cout_ext_annuel' value='<?php echo $autre_annuel;?>'/>
            <input type='hidden' name='comment' value='<?php echo $comment;?>'/>
            <input type='hidden' name='cout_externalite' value='<?php echo $externalities;?>'/>
            <input type='hidden' name='cout_externalite_courant' value='<?php echo $externalities_courant;?>'/>
            
			<button type='submit' name="action" value="sauvegarde_infos" class='icones save tall'> Enregistrer</button>
        </form>
    </p>
<?php }; ?>


