<!--
Cette page permet de comparer les dates de mises à jour des matériaux dans la table 'materiaux' et la table 'articles'
-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "maj.php")
{
	header("Location:../index.php?view=maj");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['sce']);
	
	$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
	$nom_piece=selectChamp($projet.".pieces","nom_piece","id_piece",$id_piece);
	$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);
    
    try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
                {die('Erreur : ' . $e->getMessage());}
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
	<a class='liennoir' href='index.php?view=table_articles&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Table des articles</a> >
	<a class='liennoir' href='index.php?view=maj&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Vérification des mises à jour</a>
</p>
        
<form action='controleur.php'>
    <button type="submit" name="action" value="retour_sce_id" class="icones left_arrow"> Retour au scénario</button></br>
   <center> <button type="submit" name="action" value="table_articles" class="icones tall1">Table des articles</button></center></br>
</form>
    
<p>Les articles suivants ont été mis à jour dans la base des matériaux. Cliquez sur "Mettre à jour" pour actualiser les caractéristiques de vos articles.</p>         
<p>
    <table>
        <thead>
            <tr>
				<?php
				$idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_projet);
				if ($idAuteur == $_SESSION['idUser'])
				{
				?>
                <th></th>
				<?php }; ?>
                <th>Code Matériau</th>
                <th>MAJ</th>
                <th>CUPI</th>
                <th>Poste</th>
                <th>Type</th>
                <th>Libellé</th>
                <th>Fabricant</th>
                <th>Prix unitaire</th>
                <th>Unité</th>
                <th>Durée de vie</th>
                <th>Taux entretien</th>
                <th>Taux remplacement</th>
            </tr>
        </thead>
                     
        <tbody>
                                            
        <?php   $affiche=$bdd->query('SELECT '.$projet.'.articles.*, optibuilding.materiaux.* FROM '.$projet.'.articles INNER JOIN optibuilding.materiaux ON '.$projet.'.articles.code_article = optibuilding.materiaux.code_mat
                                        WHERE id_scenario='.$id_scenario.' AND '.$projet.'.articles.MAJ_article < optibuilding.materiaux.MAJ_mat ');
        
        while($donnees=$affiche->fetch()){
        ?>
            <tr>
				<?php
				if ($idAuteur == $_SESSION['idUser'])
				{
				?>
				<td rowspan=2>
                    <form action='controleur.php'>
                        <input type='hidden' name='id_article' value='<?php echo $donnees['id_article'];?>'/>
                        <input type='hidden' name='MAJ_article' value='<?php echo $donnees['MAJ_mat'];?>'/>
                        <input type='hidden' name='CUPI_article' value='<?php echo $donnees['CUPI_mat'];?>'/>
                        <input type='hidden' name='poste' value='<?php echo $donnees['poste_mat']; ?>'/>
                        <input type='hidden' name='type_materiau' value='<?php echo $donnees['type_mat']; ?>'/>
                        <input type='hidden' name='libelle' value='<?php echo $donnees['libelle_mat']; ?>'/>
                        <input type='hidden' name='fabricant' value='<?php echo $donnees['fabricant_mat']; ?>'/>
                        <input type='hidden' name='prix_unitaire' value='<?php echo $donnees['prix_unitaire_mat']; ?>' />
                        <input type='hidden' name='unite' value='<?php echo $donnees['unite_mat']; ?>' />
                        <input type='hidden' name='duree_de_vie' value='<?php echo $donnees['duree_de_vie_mat']; ?>'/>
                        <input type='hidden' name='taux_entretien' value='<?php echo $donnees['taux_entretien_mat']; ?>'/> 
                        <input type='hidden' name='taux_remplacement' value='<?php echo $donnees['taux_remplacement_mat']; ?>'/>
                        
                        <button type="submit" name="action" value="maj_art" onclick="return confirm('Voulez-vous mettre à jour cet article ?');" class="icones small1">Mettre à jour</button>
                    </form>
                </td>
				<?php }; ?>
                <td><?php echo $donnees['code_article']; ?></td>
                <td><?php echo $donnees['MAJ_article'];?></td>
                <td><?php echo $donnees['CUPI_article'];?></td>
                <td><?php echo $donnees['poste']; ?></td>
                <td><?php echo $donnees['type_materiau']; ?></td>
                <td><?php echo $donnees['libelle']; ?></td>
                <td><?php echo $donnees['fabricant']; ?></td>
                <td><?php echo $donnees['prix_unitaire']; ?></td>
                <td><?php echo $donnees['unite']; ?></td>
                <td><?php echo $donnees['duree_de_vie']; ?></td>
                <td><?php echo $donnees['taux_entretien']; ?></td>
                <td><?php echo $donnees['taux_remplacement']; ?></td>
            </tr>

            <tr>
                <td><?php echo $donnees['code_mat']; ?></td>
                <td><?php echo $donnees['MAJ_mat'];?></td>
                <td><?php echo $donnees['CUPI_mat'];?></td>
                <td><?php echo $donnees['poste_mat']; ?></td>
                <td><?php echo $donnees['type_mat']; ?></td>
                <td><?php echo $donnees['libelle_mat']; ?></td>
                <td><?php echo $donnees['fabricant_mat']; ?></td>
                <td><?php echo $donnees['prix_unitaire_mat']; ?></td>
                <td><?php echo $donnees['unite_mat']; ?></td>
                <td><?php echo $donnees['duree_de_vie_mat']; ?></td>
                <td><?php echo $donnees['taux_entretien_mat']; ?></td>
                <td><?php echo $donnees['taux_remplacement_mat']; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</p>

<?php

if ($idAuteur == $_SESSION['idUser'])
{
	$scenario=$bdd->query('SELECT * FROM '.$projet.'.scenarios WHERE id_scenario='.$id_scenario.'');
    $tab=$scenario->fetch();
?>
<form action='controleur.php'>
	<input type='hidden' name='id_scenario' value='<?php echo $id_scenario; ?>'/>
	<input type='hidden' name='nom_scenario' value='<?php echo $tab['nom_scenario']; ?>'/>
	<input type='hidden' name='comment_scenario' value='<?php echo $tab['comment_scenario']; ?>'/>
	<center> <button type="submit" name="action" value="copier_maj" class='icones tall2 copy'> Copier le scénario avec les mises à jour</button></center></br>
</form>
<?php } ?>