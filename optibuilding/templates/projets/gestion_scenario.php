<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "gestion_scenario.php")
{
	header("Location:../index.php?view=gestion_scenario");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";
?>
         
<?php   $dbid=htmlentities($_GET['projet']);
		$piece=htmlentities($_GET['piece']);
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=projet_'.$dbid.';charset=utf8', $BDD_user, $BDD_password,
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
                
        $affiche=$bdd->query("SELECT * FROM pieces WHERE id_piece='$piece'");
        $donnees=$affiche->fetch();
        $dbname=$donnees['nom_piece'];
		$_SESSION['piece']=$piece;
		$nom_projet=selectChamp("optibuilding.projet","name","id_project",$dbid);
?>

<!-- Affichage des différentes scénarios de la pièce avec un lien vers les scénarios -->
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($dbid); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($dbid); ?>&piece=<?php echo($piece); ?>'>Scénarios de la Pièce <?php echo($dbname); ?></a>
</p>

<h1><span>Scénarios de la Pièce <?php echo($dbname); ?></span></h1><hr/>

<form action='controleur.php'>
	<?php
	$id_project=$_SESSION['projet'];
	$idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_project);
	if ($idAuteur == $_SESSION['idUser'])
	{
	?>
	<?php }; ?>
	
	<button type="submit" name="action" value="retour_pieces" class="icones left_arrow ">  Retour aux pièces</button>
</form>

<p>
        <fieldset>
            <p class='legende'>Informations sur la pièce</p>
			<label class='label6'> Surface :</label> <div class='textarea'><?php echo $donnees['surface']; ?></div></br>
            <label class='label6'> Commentaires :</label></label> <div class='textarea'>  <?php echo $donnees['comment_piece']; ?></div></br></br>
        </fieldset>
</p>

</br>		
<p>Ci dessous, retrouvez tous les scénarios créés pour cette pièce :</p>

<p><table>
    <thead><tr>
		<?php
        if ($idAuteur == $_SESSION['idUser'])
        {
        ?>
			<th></th>
		<?php }; ?>
        <th>N° scénarios</th>
		<th>Nom scénarios</th>
        <th>Commentaires</th>
    </tr></thead>
        
	<tbody>
		<?php   try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=projet_'.$dbid.';charset=utf8',  $BDD_user, $BDD_password,
							array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
				catch (Exception $e)
					{ die('Erreur : ' . $e->getMessage());}
                    
        $affiche=$bdd->query("SELECT* FROM scenarios WHERE id_piece='$piece' ORDER BY id_scenario");
            
        while($donnees=$affiche->fetch())
        {?>
            <tr>
				<?php
                if ($idAuteur == $_SESSION['idUser'])
                {
                ?>
				<td class='bouton'>
                <form action='controleur.php'>
                    <input type='hidden' name='id_scenario' value='<?php echo $donnees['id_scenario']; ?>'/>
					<input type='hidden' name='nom_scenario' value='<?php echo $donnees['nom_scenario']; ?>'/>
					<input type='hidden' name='comment_scenario' value='<?php echo $donnees['comment_scenario']; ?>'/>
					<button type="submit" name="action" value="copier_sce" class='icones copy small'> Copier</button>
                    <button type="submit" name="action" value="modif_sce" class='icones edit small'> Modifier</button>
					<button type="submit" name="action" value="retirer_sce" onclick="return confirm('Voulez-vous vraiment supprimer ce scénario ?');" class='icones delete small'> Retirer</button>
                </form>
				</td>
<!-- On atteint le code de suppression de l'entrée choisie, au moment du clic, une page d'avertissement d'affiche demande de confirmer l'action -->
                <?php }; ?>
				<td><a href='index.php?view=scenario&projet=<?php echo($dbid); ?>&piece=<?php echo($piece); ?>&sce=<?php echo $donnees['id_scenario']; ?> '> Scénario <?php echo $donnees['id_scenario']; ?></a></td>
				<td><?php echo $donnees['nom_scenario']; ?></td>
				<td><?php echo tronque($donnees['comment_scenario'],70); ?></td>
			</tr>
		<?php }; ?>
  
	</tbody> 
</table></p></br>

<form action='controleur.php'>
	<?php
	$id_project=$_SESSION['projet'];
	$idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_project);
	if ($idAuteur == $_SESSION['idUser'])
	{
	?>
	<center><button type="submit" name="action" value="nouveau_scenario" class="icones add tall1"> Ajouter un scénario</button></center></br></br>
	<?php }; ?>
	
</form>