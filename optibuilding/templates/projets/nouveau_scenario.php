<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "nouveau_scenario.php")
{
	header("Location:../index.php?view=nouveau_scenario");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";

$id_projet=$_SESSION['projet'];
$id_piece=$_SESSION['piece'];
$projet="projet_".$id_projet;
$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
$nom_piece=selectChamp($projet.".pieces","nom_piece","id_piece",$id_piece);
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=nouveau_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Créer un scénario</a>
</p>

<h1><span>Créer un nouveau scénario</span></h1>
<p>Pour créer un nouveau scénario, remplissez le formulaire suivant.</p></br>
<p class='red'>* Champs obligatoires</p></br>
<!-- Formulaire de création d'un nouveau projet -->
<form action='controleur.php' class='formulaire'>
	<fieldset>
    <p>
		<ol>
			<li>
			<label for='name'>Nom du Scénario <span class='red'>*</span></label>
			<input type='text' id='name' name='name' required='required'/></br></br>
            </li>
			
			<li>
			<label for='comment'>Commentaires</label>
			<textarea name ='comment' rows="6" cols="45"></textarea></br></br>
			</li>
			
			
			<center><button type="submit" name="action" value="creer_scenario" class="icones add formule1"> Créer le scénario</button></center>
			
		</ol>
	</p>
	</fieldset>
</form>