<!--
Cette page permet la comparaison des scénarios
-->

<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "comparaison.php")
{
	header("Location:../index.php?view=comparaison");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";

?>

<?php   $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
		$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
?>

 
    <?php
    try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
        
        $req=$bdd->query('SELECT * FROM informations');
        $donnees=$req->fetch();
        
        $taux_inflation=$donnees['taux_inflation'];
        $duree_exp=$donnees['duree_exploitation'];
        $autre=$donnees['cout_ext_fixe'];
        $autre_annuel=$donnees['cout_ext_annuel'];
        $externalities=$donnees['cout_externalite'];
        $externalities_courant=$donnees['cout_externalite_courant'];
    ?>
    
	<p class="notPrint">
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=comparaison&projet=<?php echo($id_projet); ?>'>Synthèse du projet <?php echo(utf8_encode($nom_projet)); ?></a>
	</p>
	
	<form action='controleur.php'>
		<button type="submit" name="action" value="retour_pieces" class="icones left_arrow notPrint">  Retour aux pièces</button>
	</form>
	
     <p><p class='ficheart'>Informations sur le projet</p>
        <fieldset>
            <p class='legende'>Taux</p>
            <label class='label6'>Taux d'inflation :</label><div class='textarea' ><?php if(isset($taux_inflation)){echo number_format($taux_inflation,2,"."," ");}else{echo 2.0;}?> %</div></br>
            <label class='label6'>Durée d'exploitation :</label><div class='textarea' >  <?php if(isset($duree_exp)){echo $duree_exp;}else{echo 0;}?> ans</div></br>
        </fieldset>
        
        <fieldset>
            <p class='legende'>Externalités</p>
            <label class='label6'>Coûts fixes :</label><div class='textarea' >  <?php if(isset($autre)){echo number_format($autre,2,"."," ");}else{echo 0;}?> €</div></br>
            <label class='label6'>Coûts annuels : </label><div class='textarea' > <?php if(isset($autre_annuel)){echo number_format($autre_annuel,2,"."," ");}else{echo 0;}?> €</div></br>
            </br>
            <label class='label6'>Coût des externalités : </label> <div class='textarea' ><?php echo number_format($externalities,2,"."," ");?> €</div></br>
            <label class='label6'>Coût courant des externalités : </label> <div class='textarea' ><?php echo number_format($externalities_courant,2,"."," ");?> €</div></br>
     
        </fieldset></p>
	</br></br>
     
    <p class='ficheart'>Sélectionnez les scénarios et faites la synthèse de votre projet. Le coût global affiché prend en compte les externalités.</p>
    
    <p><form method='post' action=''>
    
    <?php                
    $affiche_piece=$bdd->query('SELECT* FROM pieces ORDER BY id_piece');
    while($donnees_piece=$affiche_piece->fetch()){
        ?>
        <p><table>
            <caption><?php echo $donnees_piece['nom_piece'];?></caption>
            <thead>
                <tr>
                        <th colspan=3 ></th>
                      
                        <th class='coutbrut' colspan=3>Coût brut</th>
                        <th class='coutcourant' colspan=3>Coût courant</th>
                </tr>
                <tr>
                        <th class="scenario">Scénario</th>
                        <th>Commentaires</th>
                        <th>Construction</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                </tr>
            </thead>
        <?php
        $affiche_scenario=$bdd->query('SELECT* FROM scenarios WHERE id_piece="'.$donnees_piece['id_piece'].'" ORDER BY id_scenario');
        while($donnees_scenario=$affiche_scenario->fetch())
        {    
        $affiche_resultat=$bdd->query('SELECT* FROM resultats WHERE id_scenario="'.$donnees_scenario['id_scenario'].'" ');
        $donnees_resultat=$affiche_resultat->fetch();
        
        ?>
        <tr>
            <td>
                <input type='radio' name='<?php echo $donnees_piece['id_piece'];?>' value='<?php echo $donnees_scenario['id_scenario'];?>'
                <?php if(isset($_POST[$donnees_piece['id_piece']]) && $_POST[$donnees_piece['id_piece']]==$donnees_scenario['id_scenario']){ ?> checked='checked' <?php } ?>'/>
                <a href='index.php?view=scenario&projet=<?php echo $id_projet;?>&piece=<?php echo $donnees_piece['id_piece'];?>&sce=<?php echo $donnees_scenario['id_scenario'];?>'>
            Scenario <?php echo $donnees_scenario['id_scenario']; ?></a>
            </td>
            <td><?php echo $donnees_scenario['comment_scenario']; ?></td>
            <td><?php echo number_format($donnees_resultat['cout_construction'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnees_resultat['cout_remplacement'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnees_resultat['cout_maintenance'],2,"."," "); ?> €</td>            
            <td><?php echo number_format($donnees_resultat['cout_global'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnees_resultat['cout_remplacement_courant'],2,"."," "); ?> €</td>
            <td><?php echo number_format($donnees_resultat['cout_maintenance_courant'],2,"."," "); ?> €</td>            
            <td><?php echo number_format($donnees_resultat['cout_global_courant'],2,"."," "); ?> €</td>
        </tr>
        <?php
        }
        ?>
        </table></p>
    <?php
    }
    ?>
    <center><button type='submit' class='icones tall1 notPrint'> Synthèse </button></center>
    </form></p>
    
    <p><table>
    <caption>Synthèse du projet</caption>
        <thead>
                <tr>
                        <th colspan=3></th>
                        
                        <th class='coutbrut' colspan=3>Coût brut</th>
                        <th class='coutcourant' colspan=3>Coût courant</th>
                </tr>
                <tr>
                        <th></th>
                        <th >Scénario</th>
                        <th>Construction</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                        <th>Remplacement</th>
                        <th>Maintenance</th>
                        <th>Global</th>
                </tr>
        </thead>
        
        <tbody>
                <?php
// On commence par définir des variables qui permettent de calculer les coûts.
// Les valeurs de ces variables augmentent à chaque itération
// On prend en compte les externalités dans le cout global
                        $total_construction=0;
                        $total_remplacement=0;
                        $total_maintenance=0;
                        $total_global=$externalities;
                        $total_remplacement_courant=0;
                        $total_maintenance_courant=0;
                        $total_global_courant=$externalities_courant;
                
                        $affiche_piece=$bdd->query('SELECT* FROM pieces ORDER BY id_piece');
                        while($donnees_piece=$affiche_piece->fetch()){ ?>
                <tr>
                <?php   
// La boucle if sert à n'afficher les lignes du tableau seulement si des cases ont été sélectionnées dans le formulaire
                        if (isset($_POST[$donnees_piece['id_piece']])) {
                                $id_scenario=$_POST[$donnees_piece['id_piece']];
                
                $affiche_resultat=$bdd->query('SELECT* FROM resultats WHERE id_scenario="'.$id_scenario.'" ');
                $donnees_resultat=$affiche_resultat->fetch();
                ?>
                        <td><?php echo $donnees_piece['nom_piece'];?></td>
                        <td><?php echo $_POST[$donnees_piece['id_piece']];?></td>
                        <td><?php $total_construction+=$donnees_resultat['cout_construction'];
                                echo number_format($donnees_resultat['cout_construction'],2,"."," "); ?> €</td>
                        <td><?php $total_remplacement+=$donnees_resultat['cout_remplacement'];
                                echo number_format($donnees_resultat['cout_remplacement'],2,"."," "); ?> €</td>
                        <td><?php $total_maintenance+=$donnees_resultat['cout_maintenance'];
                                echo number_format($donnees_resultat['cout_maintenance'],2,"."," "); ?> €</td>
                        <td><?php $total_global+=$donnees_resultat['cout_global'];
                                echo number_format($donnees_resultat['cout_global'],2,"."," "); ?> €</td>
                        <td><?php $total_remplacement_courant+=$donnees_resultat['cout_remplacement_courant'];
                                echo number_format($donnees_resultat['cout_remplacement_courant'],2,"."," "); ?> €</td>
                        <td><?php $total_maintenance_courant+=$donnees_resultat['cout_maintenance_courant'];
                                echo number_format($donnees_resultat['cout_maintenance_courant'],2,"."," "); ?> €</td>
                        <td><?php $total_global_courant+=$donnees_resultat['cout_global_courant'];
                                echo number_format($donnees_resultat['cout_global_courant'],2,"."," "); ?> €</td>
                </tr>                
                <?php
                }else{
// Si la condition n'est pas vérifiée, on renvoie une ligne vide
                ?>
                        <td><?php echo $donnees_piece['nom_piece'];?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                <?php
                        }
                        }?>
                <tr>
                        <td>Total</td>
                        <td></td>
                        <td><?php if(isset($total_construction)){echo number_format($total_construction,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_remplacement)){echo number_format($total_remplacement,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_maintenance)){echo number_format($total_maintenance,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_global)){echo number_format($total_global,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_remplacement_courant)){echo number_format($total_remplacement_courant,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_maintenance_courant)){echo number_format($total_maintenance_courant,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                        <td><?php if(isset($total_global_courant)){echo number_format($total_global_courant,2,"."," "); ?> €<?php }else{ echo NULL;} ?></td>
                </tr>
        </tbody>
    </table></p></br>

<center><button class="icones imprimer notPrint" onclick="window.print();"> Imprimer</button></center>