<!--
Cette page affiche les matériaux disponibles dans la table 'materiaux', on choisit le matériau que l'on veut insérer dans la table 'articles'
relative au projet en cours.
Affichage sous forme de tableau, chaque ligne présente un bouton pour limportation d'un matériau sélectionné
-->

<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "importer_article.php")
{
	header("Location:../index.php?view=importer_article");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['sce']);
	
	$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
	$nom_piece=selectChamp($projet.".pieces","nom_piece","id_piece",$id_piece);
	$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
	<a class='liennoir' href='index.php?view=table_articles&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Table des articles</a> >
	<a class='liennoir' href='index.php?view=importer_article&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Importer un article</a>
</p>
	
<form action='controleur.php'>
	<button type="submit" name="action" value="retour_sce_id" class="icones left_arrow"> Retour au scénario</button></br>
   <center> <button type="submit" name="action" value="table_articles" class="icones tall1">Table des articles</button></center>
</form>

<p>Sélectionnez les articles à importer</p>
    
<p><table>
    <thead>
        <tr>
            <th></th>
            <th class='codemat'>Code Matériau</th>
            <th class='maj'>MAJ</th>
            <th class='cupi'>CUPI</th>
            <th class='poste'>Poste</th>
            <th class='typ'>Type Matériau</th>
            <th>Libellé</th>
            <th>Fabricant</th>
            <th class='price'>Prix unitaire</th>
            <th class='unit'>Unité</th>
            <th class='duree'>Durée de vie</th>
            <th class='tae'>Taux d'entretien annuel</th>
            <th class='tar'>Taux de remplacement</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8',  $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}

        $affiche=$bdd->query('SELECT* FROM materiaux ORDER BY code_mat');
        while($donnees=$affiche->fetch())
        {?>
        <tr>
            <td class='bouton2'>
				<button onclick="maQuantite(<?php echo $donnees['id_mat']; ?>);" name="action" value="importer_mat" class='icones small'>Importer</button> 
            </td>
            <td><?php echo $donnees['code_mat']; ?></td>
            <td><?php echo $donnees['MAJ_mat'];?></td>
            <td><?php echo $donnees['CUPI_mat'];?></td>
            <td><?php echo $donnees['poste_mat']; ?></td>
            <td><?php echo $donnees['type_mat']; ?></td>
            <td><?php echo $donnees['libelle_mat']; ?></td>
            <td><?php echo $donnees['fabricant_mat']; ?></td>
            <td><?php echo $donnees['prix_unitaire_mat']; ?></td>
            <td><?php echo $donnees['unite_mat']; ?></td>          
            <td><?php echo $donnees['duree_de_vie_mat']; ?></td>
            <td><?php echo $donnees['taux_entretien_mat']; ?></td>
            <td><?php echo $donnees['taux_remplacement_mat']; ?></td>
        </tr>       
        <?php } ?>               
    </tbody>
</table></p>

<form action="controleur.php" name="importation">
	<input type='hidden' id="formId" name="id_mat" value="" />
	<input type='hidden' name="action" value="importer_mat" />
	<input type='hidden' id="formQte" name="quantite" value="" />
</form>

<script language=javascript>
	function maQuantite(id) {
		//Affichage de la popup
		var quantite = prompt('Veuillez entrer une quantité pour cet article.\nCliquez sur "Annuler" si vous souhaitez modifier cette quantité plus tard.','');
		//On recopie la quantité dans le formulaire
		if (quantite!=null) {
           		document.getElementById('formQte').value=quantite;
		}

		//On recopie l'ID dans le formulaire
		document.getElementById('formId').value = id;
		//Et on submit le formulaire
		document.importation.submit();
	
	}
</script>