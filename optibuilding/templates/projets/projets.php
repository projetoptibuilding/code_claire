<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "projets.php")
{
	header("Location:../index.php?view=projets");
	die("");
}
include_once("libs/modele.php");
include_once "libs/config.php";
?>

<h1><span>Projets<span></h1><hr/>

<p>Ci dessous, retrouvez tous les projets en cours :</p>

<h3>Liste des projets modifiables :</h3>

    <p><table>
        <thead><tr>
			<th></th>
            <th class="nom">Nom</th>
            <th class="creation">Date de création</th>
            <th class="livraison">Date de livraison</th>
            <th class="modification">Date de modification</th>
            <th class="auteur">Auteur</th>
            <th class="adresse">Adresse</th>
            <th class="cp">Code Postal</th>
            <th>Commentaires</th>
        </tr></thead>
        
		<tbody>
             
			<!-- On se connecte à la base de données contenant la table qui contient elle même tous les projets, puis on affiche les entrées de la table dont l'auteur est la personne connectée.
			Il faudra rajouter une condition pour afficher les projets en cours d'avancements WHERE progress=(...)-->                            
			<?php   try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8',  $BDD_user, $BDD_password,
									array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
					catch (Exception $e)
						{die('Erreur : ' . $e->getMessage());}
			$idAuteur=$_SESSION["idUser"];
			$affiche=$bdd->query("SELECT* FROM projet WHERE id_author='$idAuteur' ORDER BY date_delivery DESC");                
			while($donnees=$affiche->fetch())
			{?>
				<tr>
					<td class='bouton'>
						<form action='controleur.php'>
							<input type='hidden' name='id_projet' value='<?php echo $donnees['id_project']; ?>'/>
							<button type="submit" name="action" value="modif_projet" class='icones edit small'> Modifier</button>
							<button type="submit" name="action" value="suppr_projet" onclick="return confirm('Voulez-vous vraiment supprimer ce projet ?');" class='icones delete small'> Retirer</button>
						</form>
					</td>
					<td ><a href='index.php?view=pieces&projet=<?php echo($donnees['id_project']); ?> '> <?php echo $donnees['name']; ?></a></td>
					<td><?php echo $donnees['date_creation']; ?></td>               
                	<td><?php echo $donnees['date_delivery']; ?></td>
					<td><?php echo $donnees['date_modification']; ?></td>
					<td><?php echo $donnees['id_author']; ?></td>
					<td><?php echo $donnees['adress']; ?></td>
					<td><?php echo $donnees['zip_code']; ?></td>
					<td><?php echo tronque($donnees['comment'],70); ?></td>
				</tr>
			<?php } ?>
        </tbody> 
    </table></p></br>

<h3>Liste des projets consultables :</h3>

    <p><table>
        <thead><tr>
            <th class="nomv2">Nom</th>
            <th class="creation">Date de création</th>
            <th class="livraison">Date de livraison</th>
            <th class="modification">Date de modification</th>
            <th class="auteur">Auteur</th>
            <th class="adresse">Adresse</th>
            <th class="cp">Code Postal</th>
            <th>Commentaires</th>        </tr></thead>
        
		<tbody>
             
			<!-- On se connecte à la base de données contenant la table qui contient elle même tous les projets, puis on affiche les entrées de la table dont l'auteur n'est pas la personne connectée.
			Il faudra rajouter une condition pour afficher les projets en cours d'avancements WHERE progress=(...)-->                            
			<?php   try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8',  $BDD_user, $BDD_password,
									array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
					catch (Exception $e)
						{die('Erreur : ' . $e->getMessage());}
			$idAuteur=$_SESSION["idUser"];
			$affiche=$bdd->query("SELECT* FROM projet WHERE id_author<>'$idAuteur' ORDER BY date_delivery DESC");                
			while($donnees=$affiche->fetch())
			{?>
				<tr>
					<td><a href='index.php?view=pieces&projet=<?php echo($donnees['id_project']); ?> '> <?php echo $donnees['name']; ?></a></td>
					<td><?php echo $donnees['date_creation']; ?></td>               
                	<td><?php echo $donnees['date_delivery']; ?></td>
					<td><?php echo $donnees['date_modification']; ?></td>
					<td><?php echo $donnees['id_author']; ?></td>
					<td><?php echo $donnees['adress']; ?></td>
					<td><?php echo $donnees['zip_code']; ?></td>
					<td><?php echo tronque($donnees['comment'],70); ?></td>
				</tr>
			<?php } ?>
        </tbody> 
    </table></p></br></br>
	
<form action='controleur.php'>
	<center><button type="submit" name="action" value="nouveau_projet" class="icones add tall1"> Créer un nouveau projet</button></center>
</form>