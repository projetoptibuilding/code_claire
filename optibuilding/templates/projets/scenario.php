<!--
Cette page présente les différents résultats des calculs, elle permet d'accèder à la table des articles et à la page d'analyse des résultats
-->

<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "scenario.php")
{
	header("Location:../index.php?view=scenario");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";
// On fait appel aux fonctions du fichier fonctions_cout_global.php
include_once "templates/calcul/fonctions_cout_global.php";

?>
         
<?php
$id_project=htmlentities($_GET['projet']);
$id_piece=htmlentities($_GET['piece']);
$id_sce=htmlentities($_GET['sce']);
$_SESSION['scenario']=$id_sce;
$projet='projet_'.$id_project;

$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_project);
$nom_piece=selectChamp($projet.'.pieces',"nom_piece","id_piece",$id_piece);
$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_sce);

?>

<?php
if(isset($_POST['taux_inflation']) && isset($_POST['duree_exp']) && isset($_POST['autre_annuel'])
   && isset($_POST['autre']) && isset($_POST['cout_externalite']) && isset($_POST['cout_externalite_courant']))
    {  
        $duree_exp=htmlentities($_POST['duree_exp']);
        $autre=htmlentities($_POST['autre']);
        $autre_annuel=htmlentities($_POST['autre_annuel']);
        $taux_inflation=htmlentities($_POST['taux_inflation']);
        $coeff_inflation=1+$taux_inflation/100;
        $coeff_nul=1;
        $nul=0;
        
        $externalities=htmlentities($_POST['cout_externalite']);
        $externalities_courant=htmlentities($_POST['cout_externalite_courant']);
        
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $req=$bdd->query('SELECT id_article, surface, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement FROM articles WHERE id_piece="'.$id_piece.'" AND id_scenario="'.$id_sce.'" ORDER BY id_article');

        $replace_cost=sprintf('%.2f',replace_cost($projet,$id_piece,$id_sce,$duree_exp,$coeff_nul)); 
        $replace_cost_courant=sprintf('%.2f',replace_cost($projet,$id_piece,$id_sce,$duree_exp,$coeff_inflation)); 

        $construction_cost=sprintf('%.2f',construction_cost($projet,$id_piece,$id_sce));
        
        $maintenance_cost=sprintf('%.2f',maintenance_cost($projet,$id_piece,$id_sce,$duree_exp,$coeff_nul));
        $maintenance_cost_courant=sprintf('%.2f',maintenance_cost($projet,$id_piece,$id_sce,$duree_exp,$coeff_inflation));
        
        $cout_global=sprintf('%.2f',Cout_Global($projet,$id_piece,$id_sce, $duree_exp, $coeff_nul, $nul));
        
        $cout_global_courant=sprintf('%.2f',Cout_Global_Courant($projet,$id_piece,$id_sce, $duree_exp, $coeff_inflation, $nul));
    }
	else
	{
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $req=$bdd->query('SELECT * FROM resultats WHERE id_piece='.$id_piece.' AND id_scenario='.$id_sce.'');
        $donnees=$req->fetch();
        
        $replace_cost=$donnees['cout_remplacement'];
        $replace_cost_courant=$donnees['cout_remplacement_courant'];
        $construction_cost=$donnees['cout_construction'];       
        $maintenance_cost=$donnees['cout_maintenance'];
        $maintenance_cost_courant=$donnees['cout_maintenance_courant'];
        $cout_global=$donnees['cout_global'];       
        $cout_global_courant=$donnees['cout_global_courant'];       
                    
        $req2=$bdd->query('SELECT * FROM informations');
        $donnees2=$req2->fetch();
        
        $taux_inflation=$donnees2['taux_inflation'];
        $duree_exp=$donnees2['duree_exploitation'];
        $autre=$donnees2['cout_ext_fixe'];
        $autre_annuel=$donnees2['cout_ext_annuel'];
        $externalities=$donnees2['cout_externalite'];
        $externalities_courant=$donnees2['cout_externalite_courant'];
    }
?>

<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_project); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_project); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_project); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_sce); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a>
</p>            

<h1><span>Scénario : <?php echo (utf8_encode($sce)); ?></span></h1><hr/>

<form action='controleur.php'>
	<input type='hidden' name='scenario' value='<?php echo $id_sce;?>'/>
	<button type="submit" name="action" value="retour_sce" class="icones left_arrow "> Retour aux scénarios</button></br>
	<center><button type="submit" name="action" value="table_articles" class="icones tall1">Table des articles</button>&nbsp&nbsp
	<button type="submit" name="action" value="infos_projet" class="icones tall1">Informations sur le projet</button></center></br>
</form>

<p>
	<p class='ficheart'> Informations sur le projet</p>
        <fieldset>
            <p class='legende'>Taux</p>
           <label class='label6'> Taux d'inflation :</label><div class='textarea'> <?php if(isset($taux_inflation)){echo number_format($taux_inflation,2,"."," ");}else{echo 2.0;}?> %</div></br>
            <label class='label6'>  Durée d'exploitation :</label><div class='textarea'>  <?php if(isset($duree_exp)){echo $duree_exp;}else{echo 0;}?> ans</div></br></br>
        </fieldset>
        
        <fieldset>
            <p class='legende'>Externalités</p>
            <label class='label6'>Coûts fixes :</label> <div class='textarea'> <?php if(isset($autre)){echo number_format($autre,2,"."," ");}else{echo 0;}?> €</div></br>
           <label class='label6'> Coûts annuels :</label> <div class='textarea'><?php if(isset($autre_annuel)){echo number_format($autre_annuel,2,"."," ");}else{echo 0;}?> €</div></br></br>
            </br>
           <label class='label6'> Coût des externalités :</label> <div class='textarea'> <?php echo number_format($externalities,2,"."," ");?> €</div></br>
           <label class='label6'> Coût courant des externalités :</label> <div class='textarea'> <?php echo number_format($externalities_courant,2,"."," ");?> €</div></br>
        </fieldset>
        </p>

	<p>
		<p class='ficheart' id='ancresce'>Pour ce scénario :</p>

        <form method="post" action="">
            <input type='hidden' name='projet' value="<?php echo $id_project;?>"/>
            <input type='hidden' name='piece' value='<?php echo $id_piece;?>'/>
            <input type='hidden' name='scenario' value='<?php echo $id_sce;?>'/>
            <input type='hidden' name='taux_inflation' value='<?php echo $taux_inflation;?>'/>
            <input type='hidden' name='duree_exp' value='<?php echo $duree_exp;?>'/>
            <input type='hidden' name='autre' value='<?php echo $autre;?>'/>
            <input type='hidden' name='autre_annuel' value='<?php echo $autre_annuel;?>'/>
            <input type='hidden' name='cout_externalite' value='<?php echo $externalities;?>'/>
            <input type='hidden' name='cout_externalite_courant' value='<?php echo $externalities_courant;?>'/>
        
        <center><button type='submit' class='icones calculer tall1' onclick='window.location.hash="ancresce";'> Calculer </button></center>
        </form>
        
		<?php
            $idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_project);
            if ($idAuteur == $_SESSION['idUser'])
            {
        ?>
		<form action="controleur.php">
			<input type='hidden' name='projet' value='<?php echo $id_project; ?>'/>
			<input type='hidden' name='piece' value='<?php echo $id_piece; ?>'/>
			<input type='hidden' name='scenario' value='<?php echo $id_sce; ?>'/>
			<input type='hidden' name='taux_inflation' value='<?php echo $taux_inflation;?>'/>
			<input type='hidden' name='duree_exploitation' value='<?php echo $duree_exp; ?>'/>
			<input type='hidden' name='cout_ext_fixe' value='<?php echo $autre; ?>'/>
			<input type='hidden' name='cout_ext_annuel' value='<?php echo $autre_annuel; ?>'/>
			<input type='hidden' name='cout_construction' value='<?php echo $construction_cost; ?>'/>
			<input type='hidden' name='cout_remplacement' value='<?php echo $replace_cost; ?>'/>
			<input type='hidden' name='cout_maintenance' value='<?php echo $maintenance_cost; ?>'/>
			<input type='hidden' name='cout_externalites' value='<?php echo $externalities; ?>'/>
			<input type='hidden' name='cout_global' value='<?php echo $cout_global; ?>'/>
			<input type='hidden' name='cout_remplacement_courant' value='<?php echo $replace_cost_courant; ?>'/>
			<input type='hidden' name='cout_maintenance_courant' value='<?php echo $maintenance_cost_courant; ?>'/>
			<input type='hidden' name='cout_externalites_courant' value='<?php echo $externalities_courant; ?>'/>
			<input type='hidden' name='cout_global_courant' value='<?php echo $cout_global_courant; ?>'/>
			
			<center><button type='submit' name="action" value="sauvegarde_resultats" class='icones save tall1'> Enregistrer les résultats</button></center>
		</form>
		<?php }; ?>
        
		<form action="controleur.php">
			<center><button type='submit' name="action" value="analyse" class='icones tall1'>Analyser les coûts</button></center></br></br>
		</form>
        
        <label class='label6'>Coût de construction :</label><div class='textarea'> <?php echo number_format($construction_cost,2,"."," ");?> €</div></br></br> 
        
        <label class='label6'>Coût de remplacement :</label><div class='textarea'> <?php echo number_format($replace_cost,2,"."," ");?> €</div></br>
        <label class='label6'>Coût de maintenance : </label><div class='textarea'><?php echo number_format($maintenance_cost,2,"."," ");?> €</div></br>
        </br>
       <center><span class='cb'>Coût global : <?php echo number_format($cout_global,2,"."," ");?> €</span></center></br>
        </br></br>
        <label class='label6'>Coût de remplacement :</label><div class='textarea'> <?php echo number_format($replace_cost_courant,2,"."," ");?> €</div></br>
       <label class='label6'> Coût de maintenance :</label><div class='textarea'> <?php echo number_format($maintenance_cost_courant,2,"."," ");?> €</div></br>
        </br>
       <center><span class='cb'> Coût global courant : <?php echo number_format($cout_global_courant,2,"."," ");?> €</span></center></br>
        </br></br>
     </p>