<!-- Cette page traite les infos envoyées par le formulaire de la page nouveau_projet.php
 Sur cette page, on se connecte à la bdd optibuilding, on insère le nouveau projet dans la table projet puis on crée une bdd avec un nom normalisé : "projet_[id_project]" 
 de cette manière on est sûr de ne pas avoir deux bdd avec le même nom et on évite les problèmes liés aux caractères spéciaux dans les noms de bdd ou de table.
 On peut retrouver facilement une bdd d'un projet car le nom est normalisé
 
 La page conduit à la page suivante : le gestionnaire des pièces, on fait passer l'id_project via l'URL pour la page suivante dans l'argument "dbid" (data base id)-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "creer_projet.php")
{
	header("Location:../index.php?view=creer_projet");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php try{$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8', $BDD_user, $BDD_password,
						array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
catch(Exception $e)
{die('Erreur : ' . $e->getMessage());}
	
	$p=$bdd->query('SELECT * FROM projet ORDER BY id_project DESC');
	$project=$p->fetch();
	$project_name='projet_'.$project['id_project'];
	
try{ $new_bdd= new PDO ('mysql:host='.$BDD_host.';charset=utf8', 'root', '',
						array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    $new=$new_bdd->query('CREATE DATABASE IF NOT EXISTS '.$project_name.' DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci');}
	catch(Exception $e)
	{die('Erreur : ' . $e->getMessage());}
	
try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$project_name.';charset=utf8', $BDD_user, $BDD_password,
						   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
	catch (Exception $e)
	{die('Erreur : ' . $e->getMessage());}

	$req1=$bdd->query('CREATE TABLE IF NOT EXISTS articles
					( 
					id_article INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    id_scenario INT(11) UNSIGNED,
                    id_piece INT(11) UNSIGNED,
                    code_article VARCHAR(20),
					MAJ_article DATE,
					CUPI_article VARCHAR(3),
                    surface FLOAT UNSIGNED,
                    poste VARCHAR(15),
                    type_materiau VARCHAR(25),
                    libelle TEXT,
                    fabricant VARCHAR(50),
                    unite VARCHAR(5),
                    prix_unitaire FLOAT UNSIGNED,
                    duree_de_vie FLOAT UNSIGNED,
                    taux_entretien FLOAT UNSIGNED,
                    taux_remplacement FLOAT UNSIGNED
					)
                    ENGINE=InnoDB DEFAULT CHARSET=utf8');
    //id_scenario et id_piece sont des clés étrangères
    
    $req2=$bdd->query('CREATE TABLE IF NOT EXISTS resultats
					(
					id_resultat INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					id_scenario INT(11) UNSIGNED,
					id_piece INT(11) UNSIGNED,
					cout_construction DOUBLE UNSIGNED,
					cout_remplacement DOUBLE UNSIGNED,
					cout_maintenance DOUBLE UNSIGNED,
					cout_global DOUBLE UNSIGNED,
					cout_remplacement_courant DOUBLE UNSIGNED,
					cout_maintenance_courant DOUBLE UNSIGNED,
					cout_global_courant DOUBLE UNSIGNED
					)
					ENGINE=InnoDB DEFAULT CHARSET=utf8');
    //id_scenario et id_piece sont des clés étrangères
	
	$req3=$bdd->query('CREATE TABLE IF NOT EXISTS informations
					(
					id_information INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					taux_inflation FLOAT,
					duree_exploitation FLOAT UNSIGNED,
					cout_ext_fixe DOUBLE,
					cout_ext_annuel DOUBLE,
					comment TEXT,
					cout_externalite DOUBLE UNSIGNED,                                           
					cout_externalite_courant DOUBLE UNSIGNED
					)             
					ENGINE=InnoDB DEFAULT CHARSET=utf8');
				
	//La table 'informations' n'aura qu'une seule entrée que l'on modifiera à chaque enregistrement.
	//A la création de la table, on entre une ligne vide, il suffit d'entrer un seul attribut avec la valeur 0, $nul=0
	$nul=0;
	$req4=$bdd->prepare('INSERT INTO informations(taux_inflation) VALUES(:taux_inflation)');
    $req4->execute(array('taux_inflation'=>$nul));
	
	$req5=$bdd->query('CREATE TABLE IF NOT EXISTS pieces
					(
					id_piece INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					nom_piece VARCHAR(50) NOT NULL,
					surface FLOAT NOT NULL,
					comment_piece TEXT
					)
					ENGINE=InnoDB DEFAULT CHARSET=utf8');
	
	$req6=$bdd->query('CREATE TABLE IF NOT EXISTS scenarios
					(
					id_scenario INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
					id_piece INT(11) UNSIGNED NOT NULL,
					nom_scenario VARCHAR(50) NOT NULL,
					comment_scenario TEXT
					)
					ENGINE=InnoDB DEFAULT CHARSET=utf8');
	
	$req11=$bdd->query('ALTER TABLE articles
						ADD CONSTRAINT fk_article_scenario
						FOREIGN KEY (id_scenario)
						REFERENCES '.$project_name.'.scenarios(id_scenario)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req12=$bdd->query('ALTER TABLE articles
						ADD CONSTRAINT fk_article_piece
						FOREIGN KEY (id_piece)
						REFERENCES '.$project_name.'.pieces(id_piece)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req21=$bdd->query('ALTER TABLE resultats
						ADD CONSTRAINT fk_resultat_scenario
						FOREIGN KEY (id_scenario)
						REFERENCES '.$project_name.'.scenarios(id_scenario)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req22=$bdd->query('ALTER TABLE resultats
						ADD CONSTRAINT fk_resultat_piece
						FOREIGN KEY (id_piece)
						REFERENCES '.$project_name.'.pieces(id_piece)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');
	
	$req61=$bdd->query('ALTER TABLE scenarios
						ADD CONSTRAINT fk_scenario_piece
						FOREIGN KEY (id_piece)
						REFERENCES '.$project_name.'.pieces(id_piece)
						ON DELETE CASCADE
						ON UPDATE NO ACTION');

header('Location:index.php?view=pieces&projet='.$project['id_project'].'');
?>