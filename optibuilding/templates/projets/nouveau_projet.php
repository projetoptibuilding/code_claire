<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "nouveau_projet.php")
{
	header("Location:../index.php?view=nouveau_projet");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=nouveau_projet'>Créer un projet</a>
</p>

<h1><span>Créer un nouveau projet</span></h1>
<p>Pour créer un nouveau projet, remplissez le formulaire suivant.</p></br>
<p class='red'>* Champs obligatoires</p></br>
<!-- Formulaire de création d'un nouveau projet -->
<form action='controleur.php' class="formulaire">
	<fieldset>
    <p>
		<ol>
			<li>
			<label for='name'>Nom du projet <span class='red'>*</span></label>
			<input type='text' id='name' name='name' required='required'/></br></br>
			</li>        
        
			<li>
			<label for='date_delivery'>Date de livraison <span class='red'>*</span></label>
			<input type='date' id='date_delivery' name='date_delivery' placeholder='aaaa/mm/jj'/></br></br>
			</li>
			
			<li>	
			<label for='adress'>Adresse <span class='red'>*</span></label>
			<textarea name='adress' rows="3" cols="50"></textarea></br></br>
			</li>        
        
			<li>
			<label for='zip_code'>Code Postal <span class='red'>*</span></label>
			<input type='number' id='zip_code' name='zip_code'/></br></br>
            </li>
			
			<li>
			<label for='comment'>Commentaires</label> 
			<textarea name='comment' rows="8" cols="50"></textarea></br></br>
			</li>
		
		
			<center><button type="submit" name="action" value="creer_projet" class="icones add formule"> Créer le projet</button></center>
		
		</ol>
	</p>
	</fieldset>
</form>