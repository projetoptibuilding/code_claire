<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "modif_sce.php")
{
	header("Location:../index.php?view=modif_sce");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php
$id_projet=$_SESSION['projet'];
$projet="projet_".$id_projet;
$id_piece=htmlentities($_GET['piece']);
$id_scenario=htmlentities($_GET['sce']);
$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
$nom_piece=selectChamp($projet.'.pieces',"nom_piece","id_piece",$id_piece);
$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);
        
try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
        
$affiche=$bdd->query('SELECT* FROM scenarios WHERE id_scenario='.$id_scenario.'');
$donnees=$affiche->fetch()
?>

<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=modif_sce&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Modifier le scénario <?php echo (utf8_encode($sce)); ?></a>
</p>  

<h1><span>Pour modifier un scénario</span></h1>
<p>Cette page vous permet de modifier un scénario.</p></br>
<p class='red'>* Champs obligatoires</p></br>
<!-- Formulaire de création d'un nouveau projet -->
<form action='controleur.php' class="formulaire">
	<fieldset>
    <p>
		<ol>
			
			<input type="hidden" name="id_scenario" value="<?php echo $id_scenario; ?>"/>
				
		
			<li>
			<label for='name'>Nom du Scénario <span class='red'>*</span></label>
			<input type='text' id='name' name='name' required='required' value='<?php echo $donnees['nom_scenario']; ?>'/></br></br>
			</li>        
			
			<li>
			<label for='comment'>Commentaires</label> 
			<textarea name='comment' rows="6" cols="45"><?php echo $donnees['comment_scenario']; ?></textarea></br></br>
			</li>
		
			
			<center><button type="submit" name="action" value="modifier_sce" class="icones edit formule"> Modifier</button></center>
			
		</ol>
	</p>
	</fieldset>
</form>