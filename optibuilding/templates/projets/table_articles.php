<!--
Cette page affiche tous les articles ayant été sélectionnés pour le scénario étudié
-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "table_articles.php")
{
	header("Location:../index.php?view=table_articles");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['sce']);
	
	$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
	$nom_piece=selectChamp($projet.".pieces","nom_piece","id_piece",$id_piece);
	$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
	<a class='liennoir' href='index.php?view=table_articles&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Table des articles</a>
</p>

<form action='controleur.php'>
	<button type="submit" name="action" value="retour_sce_id" class="icones left_arrow"> Retour au scénario</button>
</form>

<p><table>
    <thead>
        <tr>
			<?php
                $idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_projet);
                if ($idAuteur == $_SESSION['idUser'])
                {
                ?>
					<th></th>
			<?php }; ?>		
            <th class='codemat'>Code Matériau</th>
            <th class='maj'>MAJ</th>
            <th class='cupi'>CUPI</th>
            <th class='poste'>Poste</th>
            <th class='typ'>Type</th>
            <th>Libellé</th>
            <th class='fab'>Fabricant</th>
            <th>Quantité</th>
            <th class='price'>Prix unitaire</th>
            <th class='unit'>Unité</th>
            <th class='duree '>Durée de vie</th>
            <th class='tae'>Taux entretien</th>
            <th class='tar'>Taux remplacement</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $affiche=$bdd->query('SELECT* FROM articles WHERE id_piece="'.$id_piece.'" AND id_scenario="'.$id_scenario.'" ORDER BY code_article'); //filtrer par pièces et scénarios
        
        while($donnees=$affiche->fetch())
        {?>
        <tr>
                <?php
                $idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_projet);
                if ($idAuteur == $_SESSION['idUser'])
                {
                ?>
				<td class='bouton'>
                <form action='controleur.php'>
                    <input type='hidden' name='id_article' value='<?php echo $donnees['id_article']; ?>'/>
                    <button type="submit" name="action" value="modif_art" class='icones edit small'> Modifier</button>
                </form>
<!-- On atteint la page de modification de matériau en faisant passer l'id du matériau par l'URL -->
                <form action='controleur.php'>
                    <input type='hidden' name='id_article' value='<?php echo $donnees['id_article']; ?>'/>
                    <button type="submit" name="action" value="retirer_art" onclick="return confirm('Voulez-vous vraiment retirer cet article ?');" class='icones delete small'> Retirer</button>
                </form>
				</td>
<!-- On atteint le code de suppression de l'entrée choisie, au moment du clic, une page d'avertissement d'affiche demande de confirmer l'action -->
                <?php }; ?>
            <td><?php echo $donnees['code_article']; ?></td>
            <td><?php echo $donnees['MAJ_article'];?></td>
            <td><?php echo $donnees['CUPI_article'];?></td>
            <td><?php echo $donnees['poste']; ?></td>
            <td><?php echo $donnees['type_materiau']; ?></td>
            <td><?php echo $donnees['libelle']; ?></td>
            <td><?php echo $donnees['fabricant']; ?></td>
            <td><?php echo $donnees['surface']; ?></td>
            <td><?php echo $donnees['prix_unitaire']; ?></td>
            <td><?php echo $donnees['unite']; ?></td>          
            <td><?php echo $donnees['duree_de_vie']; ?></td>
            <td><?php echo $donnees['taux_entretien']; ?></td>
            <td><?php echo $donnees['taux_remplacement']; ?></td>
        </tr>       
        <?php } ?>
    </tbody>
</table></p>

<form action='controleur.php'>
    <?php
    $idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$id_projet);
    if ($idAuteur == $_SESSION['idUser'])
    {
    ?>
	<center><button type="submit" name="action" value="importer_art" class="icones tall2">Importer un article de la base de données</button>
    <button type="submit" name="action" value="nouvel_art" class="icones tall1">Insérer un nouvel article</button>
    <?php }; ?>
    <button type="submit" name="action" value="verif_maj" class="icones tall1">Vérifier les mises à jour</button></center>
</form>