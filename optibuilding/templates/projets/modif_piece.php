<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "modif_piece.php")
{
	header("Location:../index.php?view=modif_piece");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";
?>


<?php
$id_projet=$_SESSION['projet'];
$projet="projet_".$id_projet;
$id_piece=htmlentities($_GET['piece']);
$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
        
try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
        
$affiche=$bdd->query('SELECT* FROM pieces WHERE id_piece='.$id_piece.'');
$donnees=$affiche->fetch()
?>
<p>
	<a class="liennoir" href='index.php?view=projets'>Projets</a> >
	<a class="liennoir" href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class="liennoir" href='index.php?view=modif_piece&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Modifier la pièce <?php echo($donnees['nom_piece']); ?></a>
</p>

<h1><span>Pour modifier une pièce</span></h1>
<p>Cette page vous permet de modifier une pièce.</p></br>
<p class='red'>* Champs obligatoires</p></br>
<!-- Formulaire de création d'un nouveau projet -->
<form action='controleur.php' class="formulaire">
	<fieldset>
    <p>
		<ol>
			
			<input type="hidden" name="id_piece" value="<?php echo $id_piece; ?>"/>
				
		
			<li>
			<label for='nom_piece'>Nom de la pièce <span class='red'>*</span></label>
			<input type='text' id='nom_piece' name='nom_piece' required='required' value='<?php echo $donnees['nom_piece']; ?>'/></br></br>
			</li>        
			
			<li>    
			<label for='surface'>Surface <span class='red'>*</span></label>
			<input type='number' id='surface' name='surface' placeholder='en m^2' value='<?php echo $donnees['surface']; ?>'/></br></br>
            </li>
			
			<li>
			<label for='comment'>Commentaires </label> 
			<textarea name='comment' rows="8" cols="50" placeholder='Commentaires'><?php echo $donnees['comment_piece']; ?></textarea></br></br>
			</li>
		
			
			<center><button type="submit" name="action" value="modifier_piece" class="icones edit formule"> Modifier</button></center>
			
		</ol>
	</p>
	</fieldset>
</form>