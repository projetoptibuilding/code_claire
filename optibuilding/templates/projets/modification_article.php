<!--
Cette page affiche un formulaire de modification d'un article de la table 'articles'.
On récupère l'id du matériau via l'URL puis on affiche un formulaire prérempli avec les infos sur l'article
-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "modification_article.php")
{
	header("Location:../index.php?view=modification_article");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php   $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['sce']);
        $id_article=htmlentities($_GET['art']);
		
		$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
		$nom_piece=selectChamp($projet.".pieces","nom_piece","id_piece",$id_piece);
		$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);
		$art=selectChamp($projet.'.articles',"code_article","id_article",$id_article);
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
	<a class='liennoir' href='index.php?view=table_articles&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Table des articles</a> >
	<a class='liennoir' href='index.php?view=modification_article&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>&art=<?php echo($id_article); ?>'>Modifier l'article <?php echo(utf8_encode($art)); ?></a>
</p>

    <section>
        <p>Cette page vous permet de modifier un article dans la table.</p>
        
        <form action='controleur.php' class='formulaire2'>
            <p>
                <fieldset>
                    <?php        
                    try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                                        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
                        catch (Exception $e)
                            {die('Erreur : ' . $e->getMessage());}
        
        
                    $affiche=$bdd->query('SELECT* FROM articles WHERE id_article='.$id_article.'');
                    $mat=$affiche->fetch()
                    ?>                    
                
                    <legend>Pour modifier un matériau</legend>
                    
                    <p class='red'>* Champs obligatoires.</p>
                <ol>
				
                    <input type="hidden" name="id_article" value="<?php echo $id_article; ?>"/>
                
				<li>
                    <label class='label3' for='code_article'>Code du matériau <span class='red'>*</span></label>
                    <input type='text' id='code_article' name='code_article' value='<?php echo $mat['code_article']; ?>'/></br></br>
                 </li>
				<li>
                    <label class='label3' for='cupi'>Le matériau est-il répertorié au CUPI ?</label>
                    <input type='radio' name='cupi' value='oui' <?php if ($mat['CUPI_article'] == "oui") { ?> checked <?php }; ?>> Oui
                    <input type='radio' name='cupi' value=""> Non</br></br>
                 </li>
				<li>    
                    <label class='label3' for='poste'>Poste <span class='red'>*</span> <em>(Sol, Toiture, Vêture...)</em></label>
                    <input type='text' id='poste' name='poste' value='<?php echo $mat['poste']; ?>'/></br></br>
                 </li>
				<li>
                    <label class='label3' for='type_materiau'>Type du matériau <span class='red'>*</span></label>
                    <input type='text' id='type_materiau' name='type_materiau' value='<?php echo $mat['type_materiau']; ?>'/></br></br>
                 </li>
				<li>
                    <label class='label3' for='libelle'>Libellé <span class='red'>*</span></label>
                    <textarea name='libelle' rows="4" cols="45"><?php echo $mat['libelle']; ?></textarea></br></br>
                 </li>
				<li>
                    <label class='label3' for='fabricant'>Fabricant</label>
                    <input type='text' id='fabricant' name='fabricant' value='<?php echo $mat['fabricant']; ?>'/></br></br>
                 </li>
				<li>
                    <label class='label3' for='prix_unitaire'>Quantité</label>
                    <input type='number' id='surface' name='surface' value='<?php echo $mat['surface']; ?>' min="0" step="0.001" style="width:50px;"/>
                
                    <select name="unite" id="unite">
                        <option value="m" <?php if ($mat['unite'] == "m") { ?> selected="selected" <?php }; ?>>m</option>
						<option value="m2" <?php if ($mat['unite'] == "m2") { ?> selected="selected" <?php }; ?>>m2</option>
						<option value="unitaire" <?php if ($mat['unite'] == "unitaire") { ?> selected="selected" <?php }; ?>>unitaire</option>
                    </select></br></br>
                 </li>
				<li>
                    <label  class='label3' for='prix_unitaire'>Prix unitaire <span class='red'>*</span> </label>
                    <input type='number' id='prix_unitaire' name='prix_unitaire' value='<?php echo $mat['prix_unitaire']; ?>' min="0" step="0.01" style="width:100px;"/></br></br>
                 </li>
				<li>
                    <label class='label3' for='duree_de_vie'>Durée de vie <span class='red'>*</span></label>
                    <input type='text' id='duree_de_vie' name='duree_de_vie' value='<?php echo $mat['duree_de_vie']; ?>' style="width:100px;"/> années</br></br>
                 </li>
				<li>
                    <label class='label3' for='taux_entretien'>Taux d'entretien annuel</label>
                    <input type='number' id='taux_entretien' name='taux_entretien' value="<?php echo $mat['taux_entretien']; ?>" min="0" step="0.0001" style="width:100px;"/></br></br>
                </li>
				<li> 
                    <label class='label3' for='taux_remplacement'>Taux de remplacement</label>
                    <input type='number' id='taux_remplacement' name='taux_remplacement' value="<?php echo $mat['taux_remplacement']; ?>" min="0" step="0.0001" style="width:100px;"/></br></br>
                 </li>
				
                    <center><button type="submit" name="action" value="modifier_article" class="icones edit formule"> Modifier</button></center>
                    </br></br>
                 </ol>
					
			
                </fieldset>
                </p>
                </form>
				<form action='controleur.php'>
       <button type="submit" name="action" value="table_articles" class="icones">Annuler</button>
					<button type="submit" name="action" value="retour_sce_id" class="icones left_arrow"> Retour au scénario</button>   
				</form>
    </section>
 