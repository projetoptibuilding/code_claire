<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "nouvelle_piece.php")
{
	header("Location:../index.php?view=nouvelle_piece");
	die("");
}

include_once("libs/modele.php");
include_once "libs/config.php";

$id_projet=$_SESSION['projet'];
$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=nouvelle_piece&projet=<?php echo($id_projet); ?>'>Créer une pièce</a>
</p>

<h1><span>Créer une nouvelle pièce</span></h1>
<p>Pour créer une nouvelle pièce, remplissez le formulaire suivant.</p></br>
<p class='red'>* Champs obligatoires</p></br>
<!-- Formulaire de création d'un nouveau projet -->
<form action='controleur.php' class='formulaire'>
	<fieldset>
    <p>
		<ol>
			<li>
			<label for='nom_piece'>Nom de la pièce <span class='red'>*</span></label>
			<input type='text' id='nom_piece' name='nom_piece' required='required'/></br></br>
            </li>

			<li>    
			<label for='surface'>Surface <span class='red'>*</span></label>
			<input type='number' id='surface' name='surface' placeholder='en m2'/></br></br>
            </li>
		
			<li>
			<label for='comment'>Commentaires </label>
			<textarea name='comment' rows="8" cols="50" placeholder='Commentaires' ></textarea></br></br>
			</li>
		
			<center><button type="submit" name="action" value="creer_piece" class="icones add formule"> Créer la pièce</button></center>
			
		</ol>
	</p>
</form>