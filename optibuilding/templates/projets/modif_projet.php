<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "modif_projet.php")
{
	header("Location:../index.php?view=modif_projet");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";
?>

<?php
$id_projet=htmlentities($_GET['projet']);
        
try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8', $BDD_user, $BDD_password,
                    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
        
$affiche=$bdd->query('SELECT* FROM projet WHERE id_project='.$id_projet.'');
$donnees=$affiche->fetch()
?>

<p>
	<a class="liennoir" href='index.php?view=projets'>Projets</a> >
	<a class="liennoir" href='index.php?view=modif_projet&projet=<?php echo($id_projet); ?>'>Modifier le projet <?php echo($donnees['name']); ?></a>
</p>

<h1><span>Pour modifier un projet</span></h1>
<p>Cette page vous permet de modifier un projet.</p></br>
<p class='red'>* Champs obligatoires</p></br>
<!-- Formulaire de création d'un nouveau projet -->
<form action='controleur.php' class="formulaire">
	<fieldset>
    <p>
		<ol>
			
			<input type="hidden" name="id_projet" value="<?php echo $id_projet; ?>"/>
				
		
			<li>
			<label for='name'>Nom du projet <span class='red'>*</span></label>
			<input type='text' id='name' name='name' required='required' value='<?php echo $donnees['name']; ?>'/></br></br>
			</li>        
        
			<li>
			<label for='date_delivery'>Date de livraison <span class='red'>*</span></label>
			<input type='date' id='date_delivery' name='date_delivery' placeholder='aaaa/mm/jj' value='<?php echo $donnees['date_delivery']; ?>'/></br></br>
			</li>
			
			<li>	
			<label for='adress'>Adresse <span class='red'>*</span></label>
			<textarea name='adress' rows="3" cols="50" ><?php echo $donnees['adress']; ?></textarea></br></br>
			</li>        
        
			<li>
			<label for='zip_code'>Code Postal <span class='red'>*</span></label>
			<input type='number' id='zip_code' name='zip_code' value='<?php echo $donnees['zip_code']; ?>'/></br></br>
            </li>
			
			<li>
			<label for='comment'>Commentaires</label> 
			<textarea name='comment' rows="8" cols="50"><?php echo $donnees['comment']; ?></textarea></br></br>
			</li>
		
			
			<center><button type="submit" name="action" value="modifier_projet" class="icones edit formule"> Modifier</button></center>
			
		</ol>
	</p>
	</fieldset>
</form>