<!--
Cette page comporte un formulaire qui permet d'insérer un nouvel article dans la table 'articles'
-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "nouvel_article.php")
{
	header("Location:../index.php?view=nouvel_articles");
	die("");
}
include_once "libs/modele.php";
include_once "libs/config.php";
?>
  
<section>
        
<?php   $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['sce']);
				
		$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
		$nom_piece=selectChamp($projet.".pieces","nom_piece","id_piece",$id_piece);
		$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
	<a class='liennoir' href='index.php?view=table_articles&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Table des articles</a> >
	<a class='liennoir' href='index.php?view=nouvel_article&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Créer un article</a>
</p>

    <h2>Créer un nouvel article, qui n'existe pas dans la base de données</h2>
            
    <form action='controleur.php' class="formulaire2">
        <p>
		<ol>
        <fieldset>
            <legend>Nouvel article</legend>
            
            <p class='red'>* Champs obligatoires.</p></br>
            
			<li>
            <label class="label2" for='code_mat'>Code du matériau <span class='red'>*</span></label>
            <input type='text' id='code_mat' name='code_mat'/></br></br>
            </li>
			
            <li>
            <label class="label2" for='cupi'>Le matériau est-il répertorié au CUPI ?</label>
            <input type='radio' name='cupi' value='oui'> Oui
			<input type='radio' name='cupi' value=""> Non</br></br>
            </li>
			
            <li>
            <label class="label2" for='poste'>Poste <span class='red'>*</span><em>(Sol, Toiture, Vêture...)</em></label>
            <input type='text' id='poste' name='poste'/></br></br>
            </li>
			
            <li>
            <label class="label2" for='type_materiau'>Type du matériau <span class='red'>*</span></label>
            <input type='text' id='type_materiau' name='type_materiau'/></br></br>
            </li>
			
            <li>
            <label class="label2" for='libelle'>Libellé <span class='red'>*</span></label>
            <textarea name='libelle' rows="4" cols="45" placeholder='Libellé' ></textarea></br></br>
            </li>
			
            <li>
            <label class="label2" for='fabricant'>Fabricant</label>
            <input type='text' id='fabricant' name='fabricant'/></br></br>
            </li>
			
            <li>
            <label class="label2" for='surface'>Quantité</label>
            <input type='number' id='surface' name='surface' min="0" step="0.001" style="width:50px;"/></br></br>
            </li>
			
            <li>
            <label class="label2" for='prix_unitaire'>Prix unitaire <span class='red'>*</span> </label>
            <input type='numbre' id='prix_unitaire' name='prix_unitaire' style="width:100px;"/>
            
            <select name="unite" id="unite">
                <option value="m">m</option>
                <option value="m2" selected="selected">m2</option>
                <option value="unitaire">unitaire</option>
            </select></br></br>
            </li>
			
            <li>
            <label class="label2" for='duree_de_vie'>Durée de vie <span class='red'>*</span></label>
            <input type='number' id='duree_de_vie' name='duree_de_vie' style="width:100px;"/> années</br></br>
            </li>
			
            <li>
            <label class="label2" for='taux_entretien'>Taux d'entretien annuel</label>
            <input type='number' id='taux_entretien' name='taux_entretien' value="0.03" min="0" step="0.0001" style="width:100px;"/></br></br>
            </li>
			
            <li>
            <label  class="label2" for='taux_remplacement'>Taux de remplacement</label>
            <input type='number' id='taux_remplacement' name='taux_remplacement' value="1" min="0" step="0.0001" style="width:100px;"/></br></br>
            </li>
			
            
				<center>
            <button type="submit" name="action" value="creer_art" class="icones add formule "> Créer</button>
            <button type="reset" class="icones formule1">Remettre le formulaire à zéro</button>
				</center>
			</br></br>
			
			
        </fieldset>
		</ol>
        </p>
    </form>
    <form action='controleur.php'>
            <button type="submit" name="action" value="table_articles" class="icones">Annuler</button></br>
            <button type="submit" name="action" value="retour_sce_id" class="icones left_arrow">Retour au scénario</button>

    </form>

</section>
                             
