<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "pieces.php")
{
	header("Location:../index.php?view=pieces");
	die("");
}

include_once "libs/modele.php";
include_once "libs/config.php";

?>

<!-- On se connecte à la bdd optibuilding et la table projets pour retrouver le nom du projet sur lequel on travaille.
Pour cela on récupère l'id_project via l'URL -->            
<?php   $dbid=htmlentities($_GET['projet']);
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname=optibuilding;charset=utf8', $BDD_user, $BDD_password,
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
                
        $affiche=$bdd->query('SELECT * FROM projet WHERE id_project='.$dbid.'');
        $donnees=$affiche->fetch();
        $dbname=$donnees['name'];
		$_SESSION['projet']=$dbid;
		
		try {$bddprojet= new PDO ('mysql:host='.$BDD_host.';dbname=projet_'.$dbid.';charset=utf8', $BDD_user, $BDD_password,
							array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
			catch (Exception $e)
			{die('Erreur : ' . $e->getMessage());}
		
		$infos=$bddprojet->query('SELECT * FROM informations');
        $donnees2=$infos->fetch();		
?>

<!-- Affichage des différentes pièces du projet avec un lien vers les scénarios -->
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($dbid); ?>'>Projet <?php echo($dbname); ?></a>
</p>

<h1><span>Projet <?php echo($dbname); ?></span></h1><hr/>

<?php if($donnees2['duree_exploitation'] ==0 || $donnees2['taux_inflation'] ==0)
{ ?>
<p class='red'>ATTENTION ! Vous devez enregistrer un taux d'inflation et une durée d'exploitation non nuls. Vous pouvez le faire en accédant aux "Informations sur le projet".</p></br>
<?php }; ?>

<form action='controleur.php'>
	<center><button type="submit" name="action" value="infos_projet" class="icones tall1">Informations sur le projet</button></center></br>
</form>

<p>
    <fieldset>
        <p class='legende'>Informations sur le projet</p>
		<label class='label6'> Adresse :</label><div class='textarea'> <?php echo $donnees['adress']; ?></div></br>
		<label class='label6'> Date de livraison :</label><div class='textarea'> <?php echo $donnees['date_delivery']; ?></div></br>
        <label class='label6'> Commentaires :</label> <div class='textarea'> <?php echo $donnees['comment']; ?></div></br></br>
		<label class='label6'> Taux d'inflation :</label><div class='textarea'> <?php echo $donnees2['taux_inflation']; ?></div></br>
		<label class='label6'> Durée d'exploitation :</label><div class='textarea'> <?php echo $donnees2['duree_exploitation']; ?></div></br></br>
    </fieldset>
</p>

<p>Ci dessous, retrouvez toutes les pièces créées pour ce projet :</p>

<p><table>
    <thead><tr>
		<?php
		$idAuteur=selectChamp("optibuilding.projet","id_author","id_project",$dbid);
		if ($idAuteur == $_SESSION['idUser'])
		{
		?>
			<th></th>
		<?php }; ?>	
        <th>Pièces</th>
		<th>Surface</th>
        <th>Commentaires</th>
    </tr></thead>
        
	<tbody>
                                        
	<?php   
	$affiche=$bddprojet->query("SELECT* FROM pieces ORDER BY id_piece");                
	while($donnees=$affiche->fetch())
	{?>
		<tr>
			<?php
            if ($idAuteur == $_SESSION['idUser'])
            {
            ?>
				<td class='bouton'>
                <form action='controleur.php'>
                    <input type='hidden' name='id_piece' value='<?php echo $donnees['id_piece']; ?>'/>
                    <button type="submit" name="action" value="modif_piece" class='icones edit small'> Modifier</button>
					<button type="submit" name="action" value="retirer_piece" onclick="return confirm('Voulez-vous vraiment supprimer cette pièce ?');" class='icones delete small'> Retirer</button>
                </form>
				</td>
            <?php }; ?>
			<td><a href='index.php?view=gestion_scenario&projet=<?php echo($dbid); ?>&piece=<?php echo $donnees['id_piece']; ?> '> <?php echo $donnees['nom_piece']; ?></a></td>
			<td><?php echo $donnees['surface']; ?></td>
			<td><?php echo tronque($donnees['comment_piece'],100); ?></td>
		</tr>
	<?php } ?>
    
	</tbody> 
</table></p></br>

<form action='controleur.php'>
<center>
	<?php
	if ($idAuteur == $_SESSION['idUser'])
	{
	?>
		<button type="submit" name="action" value="nouvelle_piece" class="icones add tall1"> Ajouter une pièce</button></br>
	<?php }; ?>
	<button type="submit" name="action" value="synthese_projet" class="icones tall1">Synthèse du projet</button>
</center>
</form>