<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) != "index.php")
{
	header("Location:../index.php");
	die("");
}

echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- **** H E A D **** -->
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>OptiBuilding</title>

	<!-- Liaisons aux fichiers css de Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
	<link href="css/sticky-footer.css" rel="stylesheet" />
	<link rel="stylesheet" href="css/design_optibuilding.css"/>
	<link rel="stylesheet" type="text/css" media="print" href="css/design_optibuilding.css" />
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	<script src="js/jquery.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	
</head>
<!-- **** F I N **** H E A D **** -->


<!-- **** B O D Y **** -->
<body>

<!-- style inspiré de http://www.bootstrapzero.com/bootstrap-template/sticky-footer --> 

<!-- Wrap all page content here -->
<div id="wrap">
  
	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="container">
		<img class="imageheaderleft" src="images/optibuilding.png" alt="" style="width:4.5%;height:9%;"/>
			<div class="navbar-header">
			
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				</button>
				
				<a class="navbar-brand" href="index.php?view=accueil">OptiBuilding</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				<!-- <li class="active"><a href="index.php?view=accueil">Accueil</a></li> -->
				<?=mkHeadLink("Accueil","accueil",$view)?>
				<?php
				// Si l'utilisateur n'est pas connecte, on affiche un lien de connexion 
				if (!valider("connecte","SESSION"))
					echo mkHeadLink("Se connecter","login",$view); 
					//echo "<li><a href=\"index.php?view=login\">Se connecter</a></li>";
				// Si l'utilisateur est valide, on affiche un lien vers la page "Mon compte"
				if (valider("connecte","SESSION") and valider("valide","SESSION"))
				{
					echo mkHeadLink("Mon compte","compte",$view);
					echo mkHeadLink("Projets","projets",$view);
					echo mkHeadLink("Mat&eacuteriaux","materiaux",$view);
				}
				// Si l'utilisateur est administrateur, on affiche un lien vers la page d'administration des utilisateurs et des conversations
				if (valider("connecte","SESSION") and valider("admin","SESSION"))
				{
					echo mkHeadLink("Administration des utilisateurs","users",$view);
				}
				?>
				</ul>
				
				<img class="imageheaderright" src="images/norevie.png" alt="" style="width:7%;height:14%;"/>
			</div><!--/.nav-collapse -->
		</div>
	</div>

<!-- Begin page content -->
<div class="container">
</body>