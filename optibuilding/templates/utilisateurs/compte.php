<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "compte.php")
{
	header("Location:../index.php?view=compte");
	die("");
}
include_once "libs/modele.php";
?>

<?php
	// Si un message est fourni dans la chaine de requete, on l'affiche en rouge
	if ($msg = valider("msg")) {
		echo "<div style=\"color:red;\"> $msg </div>";
	}
	
	$idUser=$_SESSION["idUser"];
	$nom=utf8_encode(selectChamp("optibuilding.users","nom","id",$idUser));
	$prenom=utf8_encode(selectChamp("optibuilding.users","prenom","id",$idUser));
?>

<div class="page-header">
	<h1><span>Paramètres du compte</span></h1>
</div>

<p class='red'>Tous les champs sont obligatoires.</p></br>

<p class="lead">
	
<form role="form" action="controleur.php">
	<div class="form-group">
		<label for="nom">Nom</label>
		<input type="text" class="form-control" id="nom" name="nom" value="<?php echo $nom; ?>">
	</div>
	<div class="form-group">
		<label for="prenom">Prénom</label>
		<input type="text" class="form-control" id="prenom" name="prenom" value="<?php echo $prenom; ?>">
	</div> 
	<div class="form-group">
		<label for="email">Email</label>
		<input type="text" class="form-control" id="email" name="login" value="<?php echo $_SESSION["email"]; ?>">
	</div>
	<div class="form-group">
		<label for="mdp">Nouveau mot de passe</label>
	    <input type="password" class="form-control" id="mdp" name="passe" value="<?php echo $_SESSION["passe"]; ?>">
	</div>
	<div class="form-group">
		<label for="mdp_conf">Confirmation du nouveau mot de passe</label>
		<input type="password" class="form-control" id="mdp_conf" name="passe_conf">
	</div>  
	<button type="submit" name="action" value="Modifier" class="icones edit tall">Modifier</button>
</form>

</p>