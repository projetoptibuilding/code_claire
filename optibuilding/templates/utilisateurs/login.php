<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) != "index.php")
{
	header("Location:../index.php?view=login");
	die("");
}

?>

<?php
	// Si un message est fourni dans la chaine de requete, on l'affiche en rouge
	if ($msg = valider("msg")) {
		echo "<div style=\"color:red;\"> $msg </div>";
	} 
?>

<div class="page-header">
	<h1><span>Connexion</span></h1>
</div>

<p class='red'>* Champs obligatoires</p></br>

<p class="lead">
	<h2>Déjà utilisateur</h2>
	<form role="form" action="controleur.php">
		<fieldset>
		<div class="form-group">
			<label for="email">Email <span class='red'>*</span></label>
			<input type="text" class="form-control" id="email" name="login">
		</div>
		<div class="form-group">
			<label for="pwd">Mot de passe <span class='red'>*</span></label>
			<input type="password" class="form-control" id="pwd" name="passe">
		</div>
		
		<button type="submit" name="action" value="Connexion" class="icones tall1">Connexion</button>
		</fieldset>
	</form>
</p>

<br/>

<p class="lead">
	<h2>Nouvel utilisateur</h2>
	<form role="form" action="controleur.php">
		<fieldset>
		<div class="form-group">
			<label for="nom">Nom <span class='red'>*</span></label>
			<input type="text" class="form-control" id="nom" name="nom">
		</div>
		<div class="form-group">
			<label for="prenom">Prénom <span class='red'>*</span></label>
			<input type="text" class="form-control" id="prenom" name="prenom">
		</div> 
		<div class="form-group">
			<label for="new_email">Email <span class='red'>*</span></label>
			<input type="text" class="form-control" id="new_email" name="login">
		</div>
		<div class="form-group">
			<label for="new_pwd">Mot de passe <span class='red'>*</span></label>
			<input type="password" class="form-control" id="new_pwd" name="passe">
		</div>
		<div class="form-group">
			<label for="new_pwd_conf">Confirmation du mot de passe <span class='red'>*</span></label>
			<input type="password" class="form-control" id="new_pwd_conf" name="passe_conf">
		</div>

		<button type="submit" name="action" value="creer_compte" class="icones tall1">Créer un compte</button>
		</fieldset>
	</form>
</p>