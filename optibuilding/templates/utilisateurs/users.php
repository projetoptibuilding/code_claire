<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "users.php")
{
	header("Location:../index.php?view=users");
	die("");
}

include_once("libs/modele.php");
include_once("libs/maLibUtils.php");
include_once("libs/maLibForms.php");

?>

<h1><span>Administration des utilisateurs</span></h1>

<br/>

<?php

echo "<h3>Liste des utilisateurs non valid&eacutes :</h3>";
$users = listerUtilisateurs("nv");
mkTable($users,array('nom','prenom','email'));

?>

<hr />

<h2>Changement de statut des utilisateurs</h2>

<form action="controleur.php">

<select name="idUser">
<?php
$users = listerUtilisateurs();

$idUserPrecedent = valider("idUserPrecedent");
// vaut false ou une valeur entière 

for($i=0;$i<count($users);$i++)
//foreach ($users as $dataUser)
{
	$dataUser = $users[$i];
	if ($idUserPrecedent == $dataUser["id"])
		$sel = " selected=\"selected\" ";
	else 
		$sel =""; 

	echo "<option $sel value=\"$dataUser[id]\">\n";
	echo  $dataUser["email"];	
	if ($dataUser["valide"] == 1 ) 	echo " (v)"; 	
	else echo " (nv)";
	echo "\n</option>\n"; 
}
?>
</select>

<button type="submit" name="action" value="Valider" class="icones"> Valider</button>
<button type="submit" name="action" value="Supprimer" class="icones"> Supprimer</button>
</form>

</br>
<p>
	<u><em>NOTE :</em></u> Pour réinitialiser un mot de passe d'utilisateur, utiliser l'application web phpMyAdmin. Les mots de passe
	sont stockés dans la base de données <em>'optibuilding'</em>, table <em>'users'</em>.
</p>