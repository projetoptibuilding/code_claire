<!--
Cette page présente un détail des résultats des calculs de coûts
-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "analyse.php")
{
	header("Location:../index.php?view=analyse");
	die("");
}
include_once "libs/modele.php";
include_once "templates/calcul/fonctions_cout_global.php";
include_once "libs/config.php";
?>
    
<?php

        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);;
        $id_scenario=htmlentities($_GET['sce']);
		
		$nom_projet=selectChamp("optibuilding.projet","name","id_project",$id_projet);
		$nom_piece=selectChamp($projet.'.pieces',"nom_piece","id_piece",$id_piece);
		$sce=selectChamp($projet.'.scenarios',"nom_scenario","id_scenario",$id_scenario);
        
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                                   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                {die('Erreur : ' . $e->getMessage());}
                
        $req=$bdd->query('SELECT * FROM informations');
        $donnees=$req->fetch();
        
        $duree_exp=$donnees['duree_exploitation'];
        $taux=$donnees['taux_inflation'];
?>
<p>
	<a class='liennoir' href='index.php?view=projets'>Projets</a> >
	<a class='liennoir' href='index.php?view=pieces&projet=<?php echo($id_projet); ?>'>Projet <?php echo(utf8_encode($nom_projet)); ?></a> >
	<a class='liennoir' href='index.php?view=gestion_scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>'>Scénarios de la Pièce <?php echo(utf8_encode($nom_piece)); ?></a> >
	<a class='liennoir' href='index.php?view=scenario&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Scénario : <?php echo (utf8_encode($sce)); ?></a> >
	<a class='liennoir' href='index.php?view=analyse&projet=<?php echo($id_projet); ?>&piece=<?php echo($id_piece); ?>&sce=<?php echo($id_scenario); ?>'>Analyse des coûts</a>
</p>

<p><table>
    <caption>Coût de remplacement</caption>
        <thead>
            <tr >
                <th class='colortr'></th>
                <th class='colortr' colspan=2> Coût de Remplacement</th>
                <th class='colortr'></th>
            </tr>
            <tr>
                <th>Année</th>
                <th>Brut</th>
                <th>Courant</th>
                <th>Matériaux</th>
            </tr>
        </thead>
        
        <tbody>
                
        <?php  $tableau=replace_cost_vector ($projet,$id_piece,$id_scenario,$duree_exp);
        
        foreach ($tableau as $key => $value){
                if($value!=0){
                        $n=$key;
        ?>
            <tr>
                <td><?php echo $key+1;?></td>
                <td><?php echo number_format(sprintf('%.2f',$value),2,"."," ");?> €</td>
                <td><?php echo number_format(sprintf('%.2f',$value*pow((1+$taux/100),$key)),2,"."," ");?> €</td>
                <td>
                    <?php $req1=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'" AND id_piece="'.$id_piece.'"');
                        while($donnees1=$req1->fetch())
                        {
                            if(condition_replace($donnees1['duree_de_vie'],$key+1,$duree_exp)==1)
                            {
                                $cout=$donnees1['taux_remplacement']*$donnees1['prix_unitaire']*$donnees1['surface'];
                                $cout_courant=$cout*pow((1+$taux/100),$key);
                                ?>
                                <a href='index.php?view=fiche_article&projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&sce=<?php echo $id_scenario;?>&mat=<?php echo $donnees1['id_article'];?>'>
                                <?php echo $donnees1['code_article'];?>
                                </a>
                                <?php echo '   '.'('.number_format(sprintf('%.2f',$cout),2,"."," ").' € || '.number_format(sprintf('%.2f',$cout_courant),2,"."," ").' €)   ; ';
                            }
                        }
                    ?>
                </td>
            </tr>
        <?php   }
        }; ?>
        </tbody>
</table></p>

</br></br>

    <p><table>
    <caption>Analyse par matériau</caption>
        <thead>
            <tr>
                <th class='colortr' colspan=2></th>
                <th class='colortr' colspan=3>Brut</th>
                <th class='colortr' colspan=3>Courant</th>
            </tr>
            <tr>
                <th>Matériau</th>
                <th>Construction</th>
                <th>Maintenance</th>
                <th>Remplacement</th>
                <th>Total</th>
                <th>Maintenance</th>
                <th>Remplacement</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
        <?php $req3=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'" AND id_piece="'.$id_piece.'" ORDER BY code_article');
            while($donnees3=$req3->fetch())
            {
                $construction=number_format(sprintf('%.2f',$donnees3['prix_unitaire']*$donnees3['surface']),2,"."," ");
                $maintenance=number_format(sprintf('%.2f',$donnees3['taux_entretien']*$donnees3['prix_unitaire']*$donnees3['surface']),2,"."," ");
                $maintenance_total=$maintenance*$duree_exp;
                $nb=number_replace($donnees3['duree_de_vie'],$duree_exp);
                //number_replace est une fonction que j'ai créé
                $rempl=$donnees3['taux_remplacement']*$donnees3['prix_unitaire']*$donnees3['surface'];
                $remplace=number_format(sprintf('%.2f',$rempl),2,"."," ");
                $rempl_t=$nb*$rempl;
                $remplace_total=number_format($rempl_t,2,"."," ");
                $total=number_format($construction+$maintenance_total+$remplace_total,2,"."," ");
        ?>
            <tr>
                <td><a href='index.php?view=fiche_article&projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&sce=<?php echo $id_scenario;?>&mat=<?php echo$donnees3['id_article'] ;?>'><?php echo$donnees3['code_article'] ;?></a></td>
                <td><?php   echo $construction;?> €</td>
                <!-- Couts bruts-->
                <td><?php   echo $duree_exp;?> x <?php echo $maintenance;?> = <?php echo $maintenance_total;?> €</td>
                <td><?php   echo $nb;?> x <?php echo $remplace;?> = <?php echo $remplace_total;?> €</td>
                <td><?php   echo $total;?> €</td>
                
                <!-- Couts courants-->
                <td><?php   $maitenance_total_courant=$maintenance*(1-pow((1+$taux/100),$duree_exp))/(1-(1+$taux/100));
                            echo number_format($maitenance_total_courant,2,"."," ");?> €
                </td>
                <td><?php   $remplace_total_courant=0;
                            for($i=1;$i<=$nb;$i++){
                            $remplace_total_courant+=$rempl*pow((1+$taux/100),($i*($donnees3['duree_de_vie']-1)));}
                            echo number_format($remplace_total_courant,2,"."," ");?> €
                </td>
                <td><?php   $total_courant=$construction+$maitenance_total_courant+$remplace_total_courant;
                            echo number_format($total_courant,2,"."," ");?> €
                </td>
            </tr>
        <?php }; ?>
        </tbody>
    </table></p>
    </br></br>
    <form action='controleur.php'>          
        <button type="submit" name="action" value="retour_sce_id" class="icones left_arrow"> Retour</button>
    </form>
