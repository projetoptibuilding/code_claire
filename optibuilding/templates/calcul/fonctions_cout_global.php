<!--
Ce fichier contient toutes les fonctions utiles au calcul des couts globaux.
Ces fonctions sont appelées sur plusieurs pages.
Les arguments à donner sont le projet ($projet), la pièce ($piece), le scenario ($scenario) et la durée d'exploitation ($duree_exp)
-->
<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) == "fonctions_cout_global.php")
{
	header("Location:../index.php?view=fonctions_cout_global");
	die("");
}
include_once "libs/config.php";
?>

<?php
// Cout de remplacement

    // Cette fonction renvoie le vecteur durée de vie, toutes les durées de vie des matériaux sont rangées dans l'ordre dans un tableau
    function lifespan_vector($projet,$id_piece,$id_scenario)
    {
		global $BDD_host;
		global $BDD_user;
		global $BDD_password;
		
		try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                       
                       $req=$bdd->query('SELECT id_article, duree_de_vie FROM articles WHERE id_piece='.$id_piece.' AND id_scenario='.$id_scenario.' ORDER BY id_article');
//Ici l'id est bien l'id du matériau dans cette table là et non pas l'id que l'on retrouve dans la table originale
        $Dv_vector=array();
        
        while($donnes=$req->fetch()){
            array_push($Dv_vector, $donnes['duree_de_vie']); //on ajoute une entrée à la fin du tableau
        }
        return $Dv_vector;
    }
    
    //Cette fonction calcule le nombre de remplacements d'un matériau
    function number_replace($Dv,$duree_exp){
     $nb=0;
     if ($Dv!=0){
                       if(is_int($duree_exp/$Dv)){
                                              $nb=$duree_exp/$Dv-1;
                                              return $nb;
                       }else{
                                              $nb=floor($duree_exp/$Dv);
                                              return $nb;
                       }
     }else{
                       return $nb;
     }
    }
    
    // Cette fonction évalue la nécessité de remplacer un matériau selon sa durée de vie et l'année en cours.
    // Elle renvoie 1 si il faut le remplacer, 0 sinon
    function condition_replace ($Dv,$n,$duree_exp){
        //Dv : durée de vie, n : année en cours
        if($Dv!=0){
                       $c=$n/$Dv;
                       if (is_int($c) && $duree_exp!=$n)
//on regarde si le quotient est un entier et si l'année en cours est différente de la dernière année car on ne change pas un matériau la dernière année
                       {
                           return 1;
                       }else{
                           return 0;
                           }
        }else{
                       return 0;
                       }
    }
    
    // Cette fonction donne le vecteur condition, il indique la nécessité de remplacer les matériaux pour une année donnée
                       // Cette fonction peut paraitre un peu lourde puisqu'on calcule un vecteur pour chaque année
                       //mais cela sera utile pour donner un cout de remplacement pour une année donnée
    function condition_vector($projet,$id_piece,$id_scenario, $n, $duree_exp) // n est l'année que l'on étudie     
    {        
        $condition_vector= array();
        $Dv_vector=lifespan_vector($projet,$id_piece,$id_scenario);
        
        foreach ($Dv_vector as &$value){
                       array_push($condition_vector,condition_replace($value,$n,$duree_exp));
        }

        return $condition_vector;
    }
    
    // Cette fonction donne le vecteur des couts pour remplacer les matériaux
    function replace_cost_vector_per_year($projet,$id_piece,$id_scenario){
		global $BDD_host;
		global $BDD_user;
		global $BDD_password;
	
                        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $req=$bdd->query('SELECT id_article, surface, prix_unitaire, taux_remplacement FROM articles WHERE id_piece='.$id_piece.' AND id_scenario='.$id_scenario.' ORDER BY id_article'); 
        $Rc=array();
        
        while($donnes=$req->fetch()){
            $cr=$donnes['surface']*$donnes['taux_remplacement']*$donnes['prix_unitaire']; // Le cout de remplacement d'un matériau est le produit de la surface par le taux_remplacement et prix_unitaire
           array_push($Rc, $cr); //on ajoute une entrée à la fin du tableau
        }
        return $Rc;
    }
    
    //Cette fonction calcule le cout de remplacement de tous les matériaux pris en compte pour une année donnée
    // C'est la somme de tous les termes du vecteur des couts de remplacement
    function replace_cost_per_year($projet,$id_piece,$id_scenario,$n,$duree_exp) {
        $Creplace=0;
        $Cr=replace_cost_vector_per_year($projet,$id_piece,$id_scenario,$n);
        $cond=condition_vector($projet,$id_piece,$id_scenario,$n,$duree_exp);
        
         foreach ($Cr as $keyCr => $value){
                $Creplace += $value * $cond[$keyCr];
         }
        
        return $Creplace;
    }
    
    // Cette fonction donne tous les coûts de remplacement par années dans un tableau, une ligne=une année
    function replace_cost_vector ($projet,$id_piece,$id_scenario,$duree_exp){ //duree_exp est la durée exploitation
        $Rc=array();
        for($i=1;$i<=$duree_exp;$i++){
            $rcpy=replace_cost_per_year($projet,$id_piece,$id_scenario,$i,$duree_exp);
            array_push($Rc,$rcpy);
        }
        return $Rc;
    }
    

     // Fonction calculant le coût de remplacement global
    // C'est la somme des couts de remplacement de toutes les années, on multiplie par les taux
    function replace_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff){
//Sinon on fait une array_sum du vecteur cout de remplacement
        $vector=replace_cost_vector($projet,$id_piece,$id_scenario,$duree_exp);
        $rate=rate_vector($coeff,$duree_exp);
        $cost=0;
        foreach($vector as $keyvector => $value){
                       //if($value!=0){ // Si la valeur est non nulle, on multiplie cette valeur par le taux de l'année correspondante et on incrémente
                               $cost += $value*$rate[$keyvector]; // On ne calcule que les taux dont on a besoin          
                       //}else{
                       //        $cost +=0; // Si la valeur est nulle, il ne se passe rien
                       //}
        }
        return $cost;
    }
    
    
// Cout de construction

    // Cette fonction donne le vecteur cout de construction des matériaux
    function construction_cost_vector($projet,$id_piece,$id_scenario){
		global $BDD_host;
		global $BDD_user;
		global $BDD_password;
		
            try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
            $req=$bdd->query('SELECT id_article, surface, prix_unitaire FROM articles WHERE id_piece='.$id_piece.' AND id_scenario='.$id_scenario.' ORDER BY id_article'); 
            $Cc=array();//Construction cost
            
            while($donnes=$req->fetch()){
                $cost=$donnes['surface']*$donnes['prix_unitaire']; // Le cout de construction d'un matériau est le produit de la surface et prix_unitaire
                array_push($Cc, $cost);  //on ajoute une entrée à la fin du tableau
            }
            return $Cc;
        }
        
    // Je choisis de diviser la fonction de calcul de cout de construction. Comme ça  on peut avoir accès au vecteur des coûts de constructions si besoin.
        
        // Cette fonction fait la somme des couts de construction de tous les matériaux
        function construction_cost($projet,$id_piece,$id_scenario){
            $cost_vector=construction_cost_vector($projet,$id_piece,$id_scenario);
            return array_sum($cost_vector);
        }
        
        
// Cout de maintenance

    // Cette fonction renvoie le vecteur des couts de maintenance des matériaux. Ces couts de maintenance sont évalués sur une année et sont supposés constants dans le temps
    function maintenance_cost_vector($projet,$id_piece,$id_scenario){
		global $BDD_host;
		global $BDD_user;
		global $BDD_password;
		
        try {$bdd= new PDO ('mysql:host='.$BDD_host.';dbname='.$projet.';charset=utf8', $BDD_user, $BDD_password,
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $req=$bdd->query('SELECT id_article, surface, prix_unitaire, taux_entretien FROM articles WHERE id_piece='.$id_piece.' AND id_scenario='.$id_scenario.' ORDER BY id_article');
        $Mc=array();//Maintenance cost
        
        while($donnes=$req->fetch()){
            $cost=$donnes['surface']*$donnes['prix_unitaire']*$donnes['taux_entretien']; // Le cout de maintenance d'un matériau est le produit de la surface, prix_unitaire et taux_entretien
            array_push($Mc, $cost);  //on ajoute une entrée à la fin du tableau
        }
        return $Mc;
    }
    
    // Cette fonction fait la somme des couts de maintenance que l'on multiplie par la durée d'exploitation
    function maintenance_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff){
        $cost_vector=maintenance_cost_vector($projet,$id_piece,$id_scenario);
        $rate=rate_vector($coeff,$duree_exp);
        $cost=array_sum($rate)*array_sum($cost_vector);
        // Bidouillage, le cout de maintenance simple est constant dans le temps, ma boucle foreach ne marche pas
        
        /*foreach ($cost_vector as $keycost_vector => $value){
                $cost += $value * $rate[$keycost_vector];
         }*/
        return $cost;
    }

    
// Externalités

    // Cette fonction renvoie les coûts des externalités annuelles sur la durée d'exploitation
    function externality_cost($duree_exp, $coeff, $ext){ //$ext les externalités annuelles
        if ($coeff == 1)
            return $duree_exp*$ext;
        else
            return $ext*(1-pow($coeff,$duree_exp))/(1-$coeff);
    }

    
// Cout Global

    // Cette fonction retourne le cout global simple
    function Cout_Global($projet,$id_piece, $id_scenario, $duree_exp, $taux_nul, $ext_cost){ 
        return construction_cost($projet,$id_piece,$id_scenario)+maintenance_cost($projet,$id_piece,$id_scenario,$duree_exp,$taux_nul)+replace_cost ($projet,$id_piece,$id_scenario,$duree_exp,$taux_nul)+$ext_cost;
    }
    
    // Cette fonction retourne le cout global courant
    function Cout_Global_Courant($projet,$id_piece, $id_scenario, $duree_exp, $coeff_inflation, $ext_cost){ 
        return construction_cost($projet,$id_piece,$id_scenario)+maintenance_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff_inflation)+replace_cost ($projet,$id_piece,$id_scenario,$duree_exp,$coeff_inflation)+$ext_cost;
    }

    // Cette fonction retourne le cout global actualisé
    function Cout_Global_Actualise($projet,$id_piece, $id_scenario, $duree_exp, $coeff_inflation, $coeff_actualisation, $ext_cost){
                       $coeff=$coeff_inflation*$coeff_actualisation;
        return construction_cost($projet,$id_piece,$id_scenario)+maintenance_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff)+replace_cost ($projet,$id_piece,$id_scenario,$duree_exp,$coeff)+$ext_cost;
    }
    
    
// Détermination des taux

    //Taux d'inflation
    
    // Cette fonction renvoie un taux (inflation ou actualisation) pour une année donnée et selon un coefficient initial donné
    function rate($coeff,$n){
                       return sprintf('%.3f',pow($coeff,$n));
    }
    
    // Cette fonction renvoie un tableau dans lequel sont stockées toutes les valeurs pour un taux donné, sur la durée d'exploitation
    function rate_vector($coeff,$duree_exp){
                       $vector=array();
                       for($i=0;$i<=$duree_exp-1;$i++){
                                              $rate=rate($coeff,$i);
                                              array_push($vector,$rate);
                       }
                       return $vector;
    }
    
?>