<?php

// Si la page est appelée directement par son adresse, on redirige en passant pas la page index
if (basename($_SERVER["PHP_SELF"]) != "index.php")
{
	header("Location:../index.php");
	die("");
}

?>

</div>
<!-- fin du content --> 

<!-- fin du wrap -->
</div>

<div id="footer" class="notPrint">
	<div class="container">
		<p class="text-muted credit" style="float: left">
			<?php
			// Si l'utilisateur est connecte, on affiche un lien de deconnexion 
			if (valider("connecte","SESSION") and !valider("valide","SESSION"))
			{
				echo "Votre compte est en attente de validation. ";
				echo "<a class='liennoir' href=\"controleur.php?action=Logout\">Se D&eacuteconnecter</a>";
			}
			else if (valider("connecte","SESSION") and valider("valide","SESSION"))
			{
				echo "Utilisateur <b>$_SESSION[email]</b> connect&eacute depuis <b>$_SESSION[heureConnexion]</b> &nbsp; "; 
				echo "<a class='liennoir' href=\"controleur.php?action=Logout\">Se D&eacuteconnecter</a>";
			}
			?>
		</p>
		
		<p class="text-muted credit" style="float: right">
			<a class='liennoir' href="guide_utilisateur.html" target="_blank">Guide d'utilisation</a>&nbsp&nbsp
			<a class='liennoir' href="QSN.html" target="_blank">Qui sommes-nous ?</a>

		</p>
	</div>
</div>

